from rest_framework import serializers
from fintech.models import *


class SponsorSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField()
    def get_city(self, obj):
        if obj.city != None:
            return obj.city.city
        return "None"
    class Meta:
        model=Sponsor 
        fields=('name','location', 'phone', 'link', 'city')

class VoucherSerializer(serializers.ModelSerializer):

    sponsor = serializers.CharField(required=False)
    class Meta:
        model=Voucher 
        fields=('name', 'id','quantity','available','sponsor','shortcut')

class PublicEventSerializer(serializers.ModelSerializer):
    joinedList = []
    participatedList = []

    joined = serializers.SerializerMethodField()
    participated = serializers.SerializerMethodField()
    organizer = serializers.SerializerMethodField()
    imageFile = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super(PublicEventSerializer, self).__init__(*args, **kwargs)
        request = self.context['request']
        self.joinedList = list(Joining.objects.filter(user=request.user).values_list('event', flat=True))
        if Participant.objects.filter(user=request.user).exists():
            self.participatedList = list(Participant.objects.get(user=request.user).events.all().values_list('id', flat=True))

    def get_imageFile(self, obj):
        if obj.imageFile != None:
            return obj.imageFile.image.url 
        else:
            return ""

    def get_organizer(self, obj):
        if obj.organizer != None:
            return obj.organizer.name
        return "None"

    def get_joined(self, obj):
        if obj.id in self.joinedList:
            return True
        return False
    def get_participated(self, obj):
        if obj.id in self.participatedList:
            return True
        return False

    class Meta:
        model=Event 
        fields=('name', 'organizer' , 'id','location', 'startTime', 'endTime', 'joining', 'joined', 'participated','text', 'canceled' , 'imageFile', 'participant')

class EventSerializer(serializers.ModelSerializer):
    vouchers = serializers.SerializerMethodField()
    imageFile = serializers.SerializerMethodField()
    def get_imageFile(self, obj):
        if obj.imageFile != None:
            return obj.imageFile.image.url 
        else:
            return ""
    def get_vouchers(self, obj):
        c=[]
        for i in (obj.vouchers.all()):
            c.append({'id': i.id , 'name': i.name})
        return c
    
    class Meta:
        model=Event 
        fields=('name', 'id','location','uuid', 'startTime', 'endTime','participant', 'joining', 'text', 'canceled', 'vouchers', 'imageFile' )
class ParticipantSerializer(serializers.ModelSerializer):
    
    class Meta:
        model=Participant 
        fields=('full_name', 'email' )

class UserParticipantSerializer(serializers.ModelSerializer):
    vouchers = serializers.SerializerMethodField()
    events = serializers.SerializerMethodField()
    def get_events(self, obj):
        c=[]
        for i in (obj.events.all()):
            c.append({'id': i.id , 'name': i.name, 'location': i.location, 'startTime': i.startTime, 'endTime': i.endTime})
        return c

    def get_vouchers(self, obj):

        c=[]
        for i in (obj.participantvoucher_set.all()):
            c.append({'id': i.id , 'voucher_id': i.voucher.id, 'voucher_name': i.voucher.name, 'token': i.token, 'sponsor': i.voucher.sponsor.name, 'granted': i.granted})
        return c
    class Meta:
        model=Participant 
        fields=('full_name', 'email' , 'vouchers' , 'events')


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields='__all__'


class EventVoucherSerializer(serializers.ModelSerializer):
    event = serializers.SerializerMethodField()
    voucher = serializers.SerializerMethodField()
    def get_voucher(self, obj):
        if obj.voucher != None:
            return obj.voucher.name
        return "None"   
        
    def get_event(self, obj):
        if obj.event != None:
            return obj.event.name
        return "None"

    class Meta:
        model = EventVoucher
        fields=('id', 'participantNumber', 'voucher', 'event')