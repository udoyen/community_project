from django.shortcuts import render, get_object_or_404, get_list_or_404
from fintech.models import *
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import *
from rest_framework import status
from PIL import Image
from rest_framework.exceptions import ParseError
from rest_framework.parsers import FileUploadParser

from rest_framework.permissions import BasePermission
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from .authentication import FirebaseAuthentication
from rest_framework_jwt.utils import (
    check_payload,
    check_user,
    get_username_field,
    unix_epoch,
)
from django.contrib.auth.models import User
class ImageUploadParser(FileUploadParser):
    media_type = 'images/*'

class CustomObtainAuthToken(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request, *args, **kwargs):
        return Response({},status=201)

    
    def post(self, request, *args, **kwargs):
        user = FirebaseAuthentication.authenticate(self,request=request)
        if not user:
            msg = _('Unable to log in with provided credentials.')
            raise serializers.ValidationError(msg)
        payload = JSONWebTokenAuthentication.jwt_create_payload(user[0])

        response_data = {
            'token': JSONWebTokenAuthentication.jwt_encode_payload(payload),
            'user': user[0],
            'issued_at': payload.get('iat', unix_epoch())
        }
        return Response(response_data, status=status.HTTP_201_CREATED)

# Custom permission for users with "is_active" = True.
class IsOrganizer(BasePermission):
    """
    Allows access only to "is_active" users.
    """
    def has_permission(self, request, view):
        try:
            return request.user.is_authenticated and request.user.profile.role=='Organizer'
        except:
            return Response(status=401)

class AuthAPI(APIView):
    authentication_classes = [JSONWebTokenAuthentication]


    def get(self, request,):
        if request.user.profile.role=='Organizer':
            return Response({},status=201)
        return Response({},status=401)

class EventVoucherAPI(APIView):

    def get(self, request,):
        sponsor = get_object_or_404(Sponsor, user=request.user)
        vouchers = sponsor.vouchers.all()
        voucher_list=[]
        for voucher in vouchers:
            if voucher.eventvoucher_set.exists():
                voucher_list= voucher_list + list(voucher.eventvoucher_set.all())
        serializer = EventVoucherSerializer(voucher_list, many=True)
        return Response(serializer.data, status=200)
     

class SponsorAuthAPI(APIView):
    authentication_classes = [JSONWebTokenAuthentication]


    def get(self, request,):
        if request.user.profile.role=='Sponsor':
            return Response({},status=201)
        return Response({},status=401)

class GrantVoucherEvent(APIView):
    def post(self, request):


        # allow only sponsor to grant vouchers
        if 'voucher' in request.data and  'event' in request.data and  isinstance(request.data['voucher'], int) and isinstance(request.data['event'], int):
            voucher = get_object_or_404(Voucher,pk=request.data["voucher"],sponsor__user=request.user )
            event = get_object_or_404(Event,pk=request.data["event"])
            if event.participant ==0:
                        return Response({'message':'There is no Participant'},status=status.HTTP_406_NOT_ACCEPTABLE)
            if voucher.available >= event.participant and  event.participant !=0:
                for participant in event.participants.all():
                    #participant.vouchers.add(voucher)
                    ParticipantVoucher.objects.create(participant=participant, voucher=voucher)
                
                EventVoucher.objects.create(event=event, voucher=voucher)

                voucher.available = voucher.available - event.participants.count()
                voucher.save()

                return Response({'message':'Successfully Done'},status=status.HTTP_200_OK)
        
        elif 'voucher_list' in request.data and  'event' in request.data and isinstance(request.data['event'], int):
            event = get_object_or_404(Event,pk=request.data["event"])
            if event.participant ==0:
                        return Response({'message':'There is no Participant'},status=status.HTTP_406_NOT_ACCEPTABLE)
            vouchers=[]
            for voucher_id in request.data['voucher_list']:
                voucher = get_object_or_404(Voucher,pk=voucher_id,sponsor__user=request.user )
                if voucher.available < event.participant:
                    return Response({'message':'No Enough Vouchers'},status=status.HTTP_406_NOT_ACCEPTABLE)
                
                
                vouchers.append(voucher)
            
            participants = event.participants.all()
            for voucher in vouchers:
                EventVoucher.objects.create(event=event, voucher=voucher, participantNumber=event.participant)
                for participant in participants:
                    #participant.vouchers.add(voucher)
                    ParticipantVoucher.objects.create(participant=participant, voucher=voucher)
                    
                voucher.available = voucher.available - event.participant
                voucher.save()

            return Response({'message':'Successfully Done'},status=status.HTTP_200_OK)
        return Response({'message':'NOT ACCEPTABLE'},status=status.HTTP_406_NOT_ACCEPTABLE)

class VouchersAvailableAPI(APIView):

    def get(self, request, pk=None, format='json'):

        vouchers = Voucher.objects.filter(available__gt = 0)
        serializer = VoucherSerializer(vouchers, many=True)
        return Response(serializer.data)

class VouchersAPI(APIView):


    def get(self, request, pk=None, format='json'):
        if pk:
            vouchers = Voucher.objects.filter(sponsor__user=request.user, pk=pk)
        else:
            vouchers = Voucher.objects.filter(sponsor__user=request.user)

        serializer = VoucherSerializer(vouchers, many=True)
        return Response(serializer.data)
        
    def post(self, request):
        
        sponsor = get_object_or_404(Sponsor, user=request.user)
        data = request.data
        print(data)
        serializer = VoucherSerializer(data=data )
        if serializer.is_valid():
            serializer.save(sponsor = sponsor)
            return Response(serializer.data, status=status.HTTP_200_OK)
        print(serializer.errors)
        return Response({"message": "Bad Request"}, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, pk):

        voucher = get_object_or_404(Voucher, sponsor__user=request.user, pk=pk)
        voucher.delete()
        return Response({"message": "Done"},status=204)

class VoucherModifyAPI(APIView):

    def post(self, request, pk=None, format='json'):
        if pk:
            if 'amount' in request.data and isinstance(request.data['amount'], int):
                voucher = get_object_or_404(Voucher,pk=pk,sponsor__user=request.user)
                if (request.data['amount'] + voucher.available) < 0:
                    return Response({},status=status.HTTP_400_BAD_REQUEST)
                voucher.quantity = voucher.quantity + request.data['amount']
                voucher.available = voucher.available + request.data['amount']
                voucher.save()
                return Response(VoucherSerializer(instance=voucher).data,status=status.HTTP_200_OK)
        return Response({},status=status.HTTP_400_BAD_REQUEST)


class SponsorsAPI(APIView):

    def get(self, request, pk, format='json'):
        print(pk)
        voucher = get_object_or_404(Voucher, pk=pk)
        serializer = SponsorSerializer(voucher.sponsor)
        return Response(serializer.data)


from rest_framework import pagination
class CustomPagination(pagination.PageNumberPagination):
    page_size = 4
    page_size_query_param = 'page_size'
    max_page_size = 50
    page_query_param = 'page_number'

from django.db.models import Q


from rest_framework.generics import ListAPIView

class Cities(ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer



class PublicEventsAPI(APIView,CustomPagination ):
    #TODO: may add auth in public events
    #authentication_classes = [JSONWebTokenAuthentication]
    pagination_class = CustomPagination

    def get(self, request, pk=None, format='json'):
        qList=[]
        try:
        
            if pk:
                events = Event.objects.get(pk=pk)
                serializer = PublicEventSerializer(events, context={'request': request})
            else:
                city = request.GET.get('city', '')
                org = request.GET.get('org', '')
                if city != '':
                    qList.append(Q(city=city))
                if org !='':
                    qList.append(Q(organizer=org))
                if len(qList):
                    filter = qList[0]
                    for query in qList[1:]:
                        filter = filter & query
                    events = Event.objects.filter(filter)
                else:
                    events = Event.objects.all()
                    
                results = self.paginate_queryset(events, request, view=self)
                serializer = PublicEventSerializer(results, context={'request': request}, many=True)
                return self.get_paginated_response(serializer.data)
        
            return Response(serializer.data)
        except:
             Response(status=400)

import json
class EventsAPI(APIView):
    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = (IsOrganizer,)

    def get(self, request, pk=None, format='json'):
        try:
            if pk:
                events = get_object_or_404(Event,pk=pk, organizer__user= request.user)
                serializer = EventSerializer(events)
            else:
                events = get_list_or_404(Event, organizer__user= request.user)
                serializer = EventSerializer(events, many=True)
            
            
            return Response(serializer.data)
        except:
            return Response(status=404)
        
    def post(self, request, pk=None):
        try:
            if pk:
                event = get_object_or_404(Event, pk=pk, organizer__user= request.user)
                event.canceled = True
                event.save()
                return Response(EventSerializer(instance=event).data, status=status.HTTP_200_OK)

            data = request.data.copy()
            serializer = EventSerializer(data=json.loads(data['jsonData']))

            if serializer.is_valid():

                if 'file' in request.data:
                    f = data['file']

                    try:
                        img = Image.open(f)
                        img.verify()
                    except:
                        return Response({"detail": "Bad Image format"}, status=status.HTTP_400_BAD_REQUEST)

                    imageFile = ImageFile.objects.create(image=f)
                    event = serializer.save()
                    event.imageFile = imageFile
                    event.organizer = request.user.organizer_set.first()
                    event.save()
                else:
                    serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            print(serializer.errors)
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response(status=404)
    def delete(self, request, pk):
        try:
            event = get_object_or_404(Event, pk=pk, organizer__user= request.user)
            event.delete()
            return Response({"message": "Event with id `{}` has been deleted.".format(pk)},status=204)
        except:
            return Response(status=404)
class JoinAPI(APIView):
    def post(self, request, pk=None):
        if pk:
            event = get_object_or_404(Event, pk=pk)
            #Participant.objects.create(event=event, participant)
            _, created =Joining.objects.get_or_create(event=event, user=request.user)
            if not created:
                return Response({"message": "Already Joined"},status=201)
            event.increment_joining

            return Response({"message": "Joined"},status=201)
        return Response({},status=status.HTTP_400_BAD_REQUEST)   

class RegisterDeviceAPI(APIView):
    def post(self, request):
        try:

            obj, created = Profile.objects.get_or_create(
                    user=request.user,
                    
                )

            #decode JSON data
            data=json.dumps(request.data)
            #convert JSON data into Python data
            dict_json=json.loads(data)
            obj.registration_id=dict_json['registration_id']
            obj.save()

            return Response({"message": "Done"},status=201)
        except:
            return Response(status=400)


class ParticipantAPI(APIView):
    def post(self, request, pk=None):
        if pk:
            event = get_object_or_404(Event, pk=pk)

            if 'code' in request.data and request.data['code'] == event.uuid:
                    
                if not Participant.objects.filter(events=event, user=request.user).exists():
                    participant, created = Participant.objects.get_or_create(user=request.user)
                    participant.events.add(event)
                    event.increment_participant
                    return Response({"message": "Participated"},status=201)

                return Response({"message": "Already Participated"},status=201)
        return Response({},status=status.HTTP_400_BAD_REQUEST)


class ParticipantsAPI(APIView):

    def get(self, request, pk=None, format='json'):
        if pk:
            event = Event.objects.get(pk=pk)
            participants = Participant.objects.filter(events=event)
            serializer = ParticipantSerializer(participants, many=True)
  
            return Response(serializer.data)
        return Response({},status=status.HTTP_400_BAD_REQUEST)

class UserVoucherAPI(APIView):

    def get(self, request, format='json'):
        try:

            participant = Participant.objects.get(user=request.user)
            serializer = UserParticipantSerializer(participant)
            print(serializer.data)

            return Response(serializer.data)
        except Exception:
            return Response(status=status.HTTP_400_BAD_REQUEST)

from api.utils import *
class CheckVoucherAPI(APIView):
    authentication_classes = [JSONWebTokenAuthentication]

    def post(self, request, format='json'):
        # try:
            #TODO: granted restraction

            #participantId = request.data['participant_id']
            token = request.data['token']
            obj = request.data['id']

            sponsor = get_object_or_404(Sponsor,  user=request.user)
            obj = get_object_or_404(ParticipantVoucher, id=obj)

            if sponsor != obj.voucher.sponsor or token != obj.token:
                response = {'message': 'Not Acceptable Voucher!'}
                status = 406
                return Response( response, status=status)
            if obj.granted:
                response = {"message": "Already Granted!"}
                status= 406
                return Response( response, status=status)

            obj.granted = True
            obj.save()
            response = {'full_name': obj.participant.full_name, 'voucher': obj.voucher.name}
            #TODO: required to add two json.decode
            print(obj.participant.user.profile.registration_id)
            print(sponsor.user.profile.registration_id)
            jsonResponse = json.dumps(response)
            sendPushMessage = SendPushMessage()
            sendPushMessage.send_message(obj.participant.user.profile.registration_id, "Success","The Voucher was Accepted")
            sendPushMessage.send_message(sponsor.user.profile.registration_id, "Success","The Voucher now is Granted")


            return Response(jsonResponse)
"""         except Exception:
            return Response( {"message": "Bad Request!"}, status=400) """