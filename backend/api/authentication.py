import os
import firebase_admin
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
from firebase_admin import auth
from firebase_admin import credentials
from rest_framework   import authentication
from rest_framework  import exceptions

from .exceptions import FirebaseError
from .exceptions import InvalidAuthToken
from .exceptions import NoAuthToken, NotRegistered
from rest_framework_jwt.views import ObtainJSONWebTokenView 
from django.contrib.auth.models import User

if not firebase_admin._apps:

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    cred = credentials.Certificate(os.path.join(BASE_DIR, "helper.json"))
    default_app =firebase_admin.initialize_app(cred)

# https://www.oscaralsing.com/firebase-authentication-in-django/
class FirebaseAuthentication(authentication.BaseAuthentication):
    def authenticate( request):
        
        auth_header = request.META.get("HTTP_JWT")
        if not auth_header:
            raise NoAuthToken("No auth token provided")
        print(f"auth_header {auth_header}")
        id_token = auth_header.split(" ").pop()
        decoded_token = None
        
        
        try:
            decoded_token = auth.verify_id_token(id_token)
        except Exception:
            print(f"firebase ok{id_token}")
            raise InvalidAuthToken("Invalid auth token")
            pass
       
        if not id_token or not decoded_token:
            return None

        try:
            email = decoded_token.get("email")
        except Exception:
            raise FirebaseError()
        try:
            user = User.objects.filter(username=email)[0]
        except Exception:
            raise NotRegistered(email)

        print("firebase ok")
        

        return (user, None)
from rest_framework import serializers
class FirebaseJSONWebTokenSerializer(serializers.Serializer):
    """
    Serializer class used to validate a username and password.

    'username' is identified by the custom UserModel.USERNAME_FIELD.

    Returns a JSON Web Token that can be used to authenticate later calls.
    """
    print("first")
    token = serializers.CharField(read_only=True)

    def __init__(self, *args, **kwargs):
        """Dynamically add the USERNAME_FIELD to self.fields."""
        super(FirebaseJSONWebTokenSerializer, self).__init__(*args, **kwargs)


    def validate(self, data):
        print("firstfirstfirstfirst")
        user,_ = FirebaseAuthentication.authenticate(self.context['request'])


        if not user:
            msg = _('Unable to log in with provided credentials.')
            raise serializers.ValidationError(msg)

        payload = JSONWebTokenAuthentication.jwt_create_payload(user)

        return {
            'token': JSONWebTokenAuthentication.jwt_encode_payload(payload),
            'user': user,
            'issued_at': payload.get('iat', unix_epoch())
        }

from rest_framework.generics import GenericAPIView

from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from .authentication import FirebaseAuthentication
from rest_framework_jwt.utils import (
    check_payload,
    check_user,
    get_username_field,
    unix_epoch,
)

from rest_framework.response import Response
from rest_framework import status
class JSONWebTokenAPIView2(GenericAPIView):
    """Base JWT auth view used for all other JWT views (verify/refresh)."""

    permission_classes = ()
    authentication_classes = ()

    serializer_class = FirebaseJSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data.get('user') or request.user
        token = serializer.validated_data.get('token')
        issued_at = serializer.validated_data.get('issued_at')
        response_data = JSONWebTokenAuthentication. \
            jwt_create_response_payload(token, user, request, issued_at)

        response = Response(response_data, status=status.HTTP_201_CREATED)

        """ if api_settings.JWT_AUTH_COOKIE:
            set_cookie_with_token(response, api_settings.JWT_AUTH_COOKIE, token) """

        return response

class FirebaseJSONWebTokenAPIView(ObtainJSONWebTokenView):

    serializer_class = FirebaseJSONWebTokenSerializer




from firebase_admin import auth

class FirebaseRegisterJSONWebTokenSerializer(serializers.Serializer):

    token = serializers.CharField(read_only=True)
    

    def __init__(self, *args, **kwargs):
        """Dynamically add the USERNAME_FIELD to self.fields."""
        super(FirebaseRegisterJSONWebTokenSerializer, self).__init__(*args, **kwargs)


    def validate(self, data):
        request = self.context['request']

        auth_header = request.META.get("HTTP_JWT")
        if not auth_header:
            raise NoAuthToken("No auth token provided")

        id_token = auth_header.split(" ").pop()
        decoded_token = None
        try:
            decoded_token = auth.verify_id_token(id_token)
        except Exception:
            raise InvalidAuthToken("Invalid auth token")
            pass
        if not id_token or not decoded_token:
            return None
        try:
            email = decoded_token.get("email")
            name = decoded_token.get("name").split(" ")

        except Exception:
            raise FirebaseError()
        user, created = User.objects.get_or_create(username=email, defaults={'first_name': name[0], 'last_name': name[1]})

        payload = JSONWebTokenAuthentication.jwt_create_payload(user)

        return {
            'token': JSONWebTokenAuthentication.jwt_encode_payload(payload),
            'user': user,
            'issued_at': payload.get('iat', unix_epoch())
        }

class FirebaseRegisterJSONWebTokenAPIView(ObtainJSONWebTokenView):

    serializer_class = FirebaseRegisterJSONWebTokenSerializer
