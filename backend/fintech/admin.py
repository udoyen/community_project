from django.contrib import admin
from .models import *
from fcm_django.models import FCMDevice
""" from accounts.models import User """

admin.site.register(Participant)
admin.site.register(Event)
admin.site.register(Profile)
admin.site.register(ParticipantVoucher)
admin.site.register(Organizer)
admin.site.register(Sponsor)
admin.site.register(Voucher)
admin.site.register(Joining)
admin.site.register(ImageFile)
admin.site.register(EventVoucher)