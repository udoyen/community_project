from django.db import models
from django.contrib.auth.models import User
#from accounts.models import User
from django.db.models.signals import pre_save
import uuid
from django.db.models.signals import pre_save, post_save

class City(models.Model):
    city = models.CharField(max_length=50)
    def __str__(self):
        return self.city

class Sponsor(models.Model):
    name = models.CharField(max_length=150, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    location = models.CharField(max_length=150, blank=True, null=True)
    phone = models.CharField(max_length=12, default="N/A", blank=True, null=True)
    city = models.ForeignKey(City, default="N/A", blank=True, null=True,on_delete=models.SET_NULL )
    link = models.CharField(max_length=100, default="N/A", blank=True, null=True)
    def __str__(self):
        return self.name

class Organizer(models.Model):
    name = models.CharField(max_length=150, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    location = models.CharField(max_length=150, blank=True, null=True)
    def __str__(self):
        return self.name

class Voucher(models.Model):
    name = models.CharField(max_length=150, blank=True, null=True)
    quantity = models.IntegerField()
    available = models.IntegerField(blank=True, null=True)
    sponsor = models.ForeignKey(Sponsor, on_delete=models.CASCADE, related_name='vouchers')
    @property
    def shortcut(self):
        return "{}  {} from '{}'".format(self.available, self.name, self.sponsor)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.id == None:
            self.available = self.quantity
        super(Voucher, self).save(*args, **kwargs)
    class Meta:
        ordering = ['-id']
        


class ImageFile(models.Model):
    image = models.ImageField(upload_to='images/')


class Event(models.Model):
    name = models.CharField(max_length=150)
    location = models.CharField(max_length=150)
    city = models.ForeignKey(City, default=1, blank=True, null=True, on_delete=models.SET_NULL)
    participant = models.IntegerField(default=0)
    joining = models.IntegerField(default=0)
    startTime = models.DateTimeField(blank=True, null=True)
    endTime = models.DateTimeField(blank=True, null=True)
    reschedule = models.BooleanField(default=False)
    canceled = models.BooleanField(default=False)
    text = models.TextField(blank=True, null=True)
    uuid = models.CharField(max_length=36, blank=True, null=True)
    vouchers = models.ManyToManyField(Voucher,through='EventVoucher')
    organizer = models.ForeignKey(Organizer, blank=True, null=True, on_delete=models.CASCADE, related_name='events')
    imageFile = models.ForeignKey(ImageFile, blank=True, null=True, on_delete=models.SET_NULL)
    @property
    def increment_joining(self):
        self.joining = self.joining + 1
        self.save()
    @property
    def increment_participant(self):
        self.participant = self.participant + 1
        self.save()  

    def __str__(self):
        return "{}".format(self.name)
    class Meta:
        ordering = ['-id']
def save_uuid(sender, instance, **kwargs):
    if(instance.id==None):
        instance.uuid=str(uuid.uuid4())

pre_save.connect(save_uuid, sender=Event)
    

class EventVoucher(models.Model):
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    participantNumber = models.IntegerField(default=0)
    def __str__(self):
        return self.voucher.name
    class Meta:
        ordering = ['-id']

class Participant(models.Model):
    first_name = models.CharField(max_length = 150)
    last_name = models.CharField(max_length = 150)
    events = models.ManyToManyField(Event, related_name="participants" )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    #vouchers = models.ManyToManyField(Voucher)
    vouchers = models.ManyToManyField(Voucher,through='ParticipantVoucher')
    @property
    def full_name(self):
        return self.user.get_full_name()
    @property
    def email(self):
        return self.user.email



    def __str__(self):
        return self.user.get_full_name()

class ParticipantVoucherManager(models.Manager):
    def create(self, **obj_data):
        obj_data['token'] =str(uuid.uuid4())
        # Now call the super method which does the actual creation
        return super().create(**obj_data)  

class ParticipantVoucher(models.Model):
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE)
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE)
    granted = models.BooleanField(default=False)
    token  = models.CharField(max_length=128, null=True)

    objects = ParticipantVoucherManager()





class Joining(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="joining_users")
    user = models.ForeignKey(User, on_delete=models.CASCADE)




class Profile(models.Model):
    ROLES = (
        ('Sponsor','Sponsor'),
        ('Organizer','Organizer'),
        ('User','User'),
    )
    user  = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    role = models.CharField(max_length=15, choices=ROLES, null=True, blank=True)
    registration_id = models.TextField(null=True, blank=True) 
    




    

