# Generated by Django 3.2.9 on 2021-12-03 18:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fintech', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='participant',
            name='vouchers',
            field=models.ManyToManyField(to='fintech.Voucher'),
        ),
    ]
