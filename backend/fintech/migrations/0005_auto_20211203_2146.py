# Generated by Django 3.2.9 on 2021-12-03 19:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fintech', '0004_participant_vouchers'),
    ]

    operations = [
        migrations.CreateModel(
            name='EventVoucher',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fintech.event')),
                ('voucher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fintech.voucher')),
            ],
        ),
        migrations.AddField(
            model_name='event',
            name='vouchers',
            field=models.ManyToManyField(through='fintech.EventVoucher', to='fintech.Voucher'),
        ),
    ]
