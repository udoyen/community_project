"""voulenteer_fintech URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import api.views as apiViews
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from django.views.generic import TemplateView
from rest_framework_jwt.blacklist.views import BlacklistView
from rest_framework_jwt.views import verify_jwt_token
from api.authentication import FirebaseJSONWebTokenAPIView, FirebaseRegisterJSONWebTokenAPIView
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet
urlpatterns = [
    path('cards/', TemplateView.as_view(template_name="cards_page.html")),
    path('admin/', admin.site.urls),
    path('api-firebase-register/', FirebaseRegisterJSONWebTokenAPIView.as_view()),
    path('api-firebase-auth/', FirebaseJSONWebTokenAPIView.as_view()),
    path('api/grant/', apiViews.GrantVoucherEvent.as_view(), name="grant"),
    path('api/participants/<int:pk>', apiViews.ParticipantsAPI.as_view()),
    path('api/join/<int:pk>', apiViews.JoinAPI.as_view()),
    path('api/check_voucher/', apiViews.CheckVoucherAPI.as_view()),
    path('api/participant_data/', apiViews.UserVoucherAPI.as_view()),
    path('api/participant/<int:pk>', apiViews.ParticipantAPI.as_view()),
    path('api/public_events/', apiViews.PublicEventsAPI.as_view()),
    path('api/public_events/<int:pk>', apiViews.PublicEventsAPI.as_view()),
    path('api/events/', apiViews.EventsAPI.as_view()),
    path('api/events/<int:pk>', apiViews.EventsAPI.as_view()),
    path('api/vouchers/', apiViews.VouchersAPI.as_view()),
    path('api/available_vouchers/', apiViews.VouchersAvailableAPI.as_view()),
    path('api/vouchers/<int:pk>', apiViews.VouchersAPI.as_view()),
    path('api/vouchers/modify/<int:pk>', apiViews.VoucherModifyAPI.as_view()),
    path('api/cities/', apiViews.Cities.as_view()),

    # add this
    
    path('api/voucher_event/', apiViews.EventVoucherAPI.as_view()),
    path('api/sponsors/<int:pk>', apiViews.SponsorsAPI.as_view()),
    path('api/devices/', apiViews.RegisterDeviceAPI.as_view(), name='create_device'),
    

    # api version 1
    #[c.name for c in get_resolver('voulenteer_fintech.api_urls').url_patterns if c.name != None]
    path('', include(('voulenteer_fintech.api_urls', 'voulenteer_fintech'), namespace='apps-urls')),
    
    path('auth/obtain_token/', obtain_jwt_token, name='obtain_jwt_token'),
    path('auth/refresh_token/', refresh_jwt_token),
    path('auth/verify_token/', verify_jwt_token),
    path('auth',apiViews.AuthAPI.as_view(), name='organizer_auth'),
    path('sponsorauth',apiViews.SponsorAuthAPI.as_view(), name='sponsor_auth'),
    path("auth/logout/", BlacklistView.as_view({"post": "create"})),
    path('index/', TemplateView.as_view(template_name="index.html")),
    path('login/', TemplateView.as_view(template_name="login.html")),
    path('organizer/home/', TemplateView.as_view(template_name="organizer_home.html")),
    path('sponsor/home/', TemplateView.as_view(template_name="sponsor_home.html")),
    path('event/', TemplateView.as_view(template_name="event.html")),
    path('participants/', TemplateView.as_view(template_name="participants.html")),
    path('participants/<int:pk>', TemplateView.as_view(template_name="participants_details.html")),
    path('event/<int:pk>', TemplateView.as_view(template_name="event_details.html")),

]

urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
