from django.urls import get_resolver
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User


class ApiTests(APITestCase):
    def test_private_urls(self):
        """
        Ensure private urls will not be accessed without token.
        """
        urls = [c for c in get_resolver('voulenteer_fintech.api_urls').url_patterns if c.name != None]
        for url in urls:
            if '/<int' in url.pattern._route:
                response = self.client.get(reverse(f'voulenteer_fintech:{url.name}',kwargs={'pk':1}))
            else:
                response = self.client.get(reverse(f'voulenteer_fintech:{url.name}'))
            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        
    def test_login(self):
        """
        Ensure we can login and access public event link.
        """
        User.objects.create_user(username="admin1", password="paa")
        response =self.client.post(reverse('obtain_jwt_token'), {'username':'admin1','password':'paa'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {response.data["token"]}')

        response = self.client.get(reverse("voulenteer_fintech:publicEvents"))
        self.assertEqual(response.status_code, status.HTTP_200_OK) 

    
