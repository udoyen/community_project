from django.contrib import admin
from django.urls import path
import api.views as apiViews


urlpatterns = [
    #[c.name for c in get_resolver('voulenteer_fintech.api_urls').url_patterns if c.name != None]
    path('api/v1/cities/', apiViews.Cities.as_view(), name="cities"),
    path('api/v1/grant/', apiViews.GrantVoucherEvent.as_view(), name="grantVoucher"),
    path('api/v1/participants/<int:pk>', apiViews.ParticipantsAPI.as_view(), name="participantListByEvent"),
    path('api/v1/join/<int:pk>', apiViews.JoinAPI.as_view(), name="joinEvent"),
    path('api/v1/check_voucher/', apiViews.CheckVoucherAPI.as_view(), name="checkVoucher"),
    path('api/v1/participant_data/', apiViews.UserVoucherAPI.as_view(), name="participantData"),
    path('api/v1/participant/<int:pk>', apiViews.ParticipantAPI.as_view(), name="participateEvent"),
    path('api/v1/public_events/', apiViews.PublicEventsAPI.as_view(), name="publicEvents"),
    path('api/v1/public_events/<int:pk>', apiViews.PublicEventsAPI.as_view(), name="publicEventDetails"),
    path('api/v1/events/', apiViews.EventsAPI.as_view(), name="eventList"),
    path('api/v1/events/<int:pk>', apiViews.EventsAPI.as_view(), name="eventDetails"),
    path('api/v1/vouchers/', apiViews.VouchersAPI.as_view(), name="voucherList"),
    path('api/v1/available_vouchers/', apiViews.VouchersAvailableAPI.as_view(), name="availableVoucherList"),
    path('api/v1/vouchers/<int:pk>', apiViews.VouchersAPI.as_view(), name="voucherDetails"),
    path('api/v1/vouchers/modify/<int:pk>', apiViews.VoucherModifyAPI.as_view(), name="modifyVoucher"),
]