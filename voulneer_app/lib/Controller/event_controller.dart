import 'dart:convert';

import 'package:get/state_manager.dart';
import 'package:voulneer_app/Model/city.dart';
import 'package:voulneer_app/Model/event.dart';
import 'package:voulneer_app/Model/participant_data.dart';
import 'package:voulneer_app/Model/sponsor.dart';
import 'package:voulneer_app/Provider/api_provider.dart';
import 'package:voulneer_app/Repository/api_repository.dart';
import 'package:http/http.dart' as http;

class ParticipantController extends GetxController {
  final ApiRepository apiRepository = ApiRepository(
    apiProvider: ApiProvider(
      httpClient: http.Client(),
    ),
  );
  Rxn<ParticipantData> participantData = Rxn<ParticipantData>();

  String loginToken = "";
  String voucherToken = "";
  String FCMToken="";
  int eventId = 0;
  Event? event;
  Map<int,int> pageNumber = {0:1};
  Map<int,int> lastPage = {0:0};
  Map<int,int> count = {0:0};

  var isLoading = true.obs;
  var cityIsLoading = true.obs;
  RxList<Event> events = RxList<Event>();
  RxMap<int, List<Event>> mapEvents = RxMap<int, List<Event>>();

  RxList<City> cities = RxList<City>();
  Sponsor sponsor = Sponsor(name: "N/A", location: "N/A", phone: "N/A", city: "N/A", link: "N/A");

  void init() {
    //getAllEventswithPage();
    getParticipantData();
    getCities();
    registerDevice();
  }
logOut() async {
  return await  apiRepository.logOut(loginToken);

}
  Future getAllEvents() async {
    isLoading(true);
    List<Event> data = await apiRepository.getAllEvents(loginToken);
    events.value = data;
    isLoading(false);
    print(events.value);
  }

  initVars(city){
    pageNumber[city]=1;
    lastPage[city]=0;
    count[city]=0;
    mapEvents[city]=[];
  }

  Future getAllEventswithPage(city) async {
    if (lastPage[city] != pageNumber[city]) {
      List data =
          await apiRepository.getAllEventswithPage(loginToken, pageNumber[city]!, city);
      if (pageNumber[city] == 1) {
        mapEvents[city] = data[1];
      } else {
         mapEvents[city]!.addAll(data[1]);
      }
      count[city] = data[0];
    }
    lastPage[city] = pageNumber[city]!;
    update();
  }


Future getCities() async {  
      List data =
          await apiRepository.getCities(loginToken);
        if (data[0]){
           cities.value = data[1];

        } else {
          cities.value = [];
        }
        update();

  }
  Future getSponsorByVoucherID(int voucherID) async {  
      List data =
          await apiRepository.getSponsorByVoucherID(voucherID, loginToken);
        if (data[0]){
          sponsor = data[1];

        } 
        update();
  }
  Future<String> joinEvent(int id) async {
    var message = await apiRepository.joinEvent(id, loginToken);
    update();
    return message;
    
  }
  Future<String> registerDevice() async {
    var message = await apiRepository.registerDevice(FCMToken, loginToken);
    return message;
    
  }
  Future<String> participateEvent(uuid) async {
    var message =
        await apiRepository.participateEvent(eventId, loginToken, uuid);
    if (message != 'Failed') getAllEvents();
    return message;
  }

  Future<ParticipantData?> getParticipantData() async {
    isLoading(true);
    participantData.value = await apiRepository.getParticipantData(loginToken);
    isLoading(false);
  }

  encodeVoucherData(id, token, name) {
    voucherToken = base64.encode(
        utf8.encode(jsonEncode({"id": id, "token": token, "voucher": name})));
  }
}
