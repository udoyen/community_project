import 'package:voulneer_app/Model/participant_data.dart';
import 'package:voulneer_app/Provider/api_provider.dart';

class ApiRepository {
  final ApiProvider apiProvider;
  ApiRepository({required this.apiProvider});
  participateEvent(int id, String loginToken, String uuid) async {
    return await apiProvider.participateEvent(id, loginToken, uuid);
  }
 joinEvent(int id, String loginToken) async {
    return await apiProvider.joinEvent(id, loginToken);
  }
  registerDevice(String token, String loginToken) async {
    return await apiProvider.registerDevice(token, loginToken);
  }
  getAllEvents(String loginToken) async {
    return await apiProvider.getAllEvents(loginToken);
  }
getAllEventswithPage(String loginToken, int pageNumber, int city) async {
    return await apiProvider.getAllEventswithPage(loginToken, pageNumber, city);
  }
  Future<ParticipantData?> getParticipantData(String loginToken) async {
    return await apiProvider.getParticipantData(loginToken);
  }

  getCities(String loginToken) async {
    return await apiProvider.getCities(loginToken);
  }
  logOut(String loginToken) async {
    return await apiProvider.logOut(loginToken);
  }

  getSponsorByVoucherID(int voucherID, String loginToken) async {
    return await apiProvider.getSponsorByVoucherID(voucherID, loginToken);
  }
}
