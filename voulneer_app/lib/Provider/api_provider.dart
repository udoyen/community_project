import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:math';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:voulneer_app/Model/city.dart';
import 'package:voulneer_app/Model/event.dart';
import 'package:voulneer_app/Model/participant_data.dart';
import 'package:voulneer_app/Model/sponsor.dart';
import 'package:voulneer_app/Views/login_page.dart';
import 'package:voulneer_app/utils/snackbar_message.dart';

const baseUrl = 'http://192.168.1.103/';

class ApiProvider {
  final http.Client httpClient;

  ApiProvider({required this.httpClient});

  Future<String> registerDevice(String token, String loginToken) async {
    try {
            var data = Map();
      data['registration_id'] = token;

    var response = await httpClient.post(Uri.parse(baseUrl + "api/devices/",),
    body: jsonEncode(data),
     headers: {'Authorization' : "Bearer $loginToken", "Content-Type": "application/json"},).timeout(const Duration(seconds: 4));
    if (response.statusCode == 201) {
      return json.decode(response.body)["message"];
    } else {
      Get.snackbar('Error', 'Failed',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
      return "Failed";
    }
  } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    }
  }

Future<String> participateEvent(int id, String loginToken, String uuid) async {
  try {
    var response = await httpClient.post(Uri.parse(baseUrl + "api/participant/$id",),
     headers: {'Authorization' : "Bearer $loginToken"},
     body: {'code': uuid}
     ).timeout(const Duration(seconds: 4));
    if (response.statusCode == 201) {
      Get.snackbar('Done', jsonDecode(response.body)['message'],
          duration: const Duration(seconds: 3),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return json.decode(response.body)["message"];
    } else {
      Get.snackbar('Error', 'Failed',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
      return "Failed";
    }
} on TimeoutException catch (_) {
      print("TimeOut Error");
      
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    }
  }
  Future<String> joinEvent(int id, String loginToken) async {
    try {
    var response = await httpClient.post(Uri.parse(baseUrl + "api/join/$id",),
     headers: {'Authorization' : "Bearer $loginToken"},).timeout(const Duration(seconds: 4));
    if (response.statusCode == 201) {
      return json.decode(response.body)["message"];
    } else {
      Get.snackbar('Error', 'Failed',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
      return "Failed";
    }
  } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    }
  }

  getAllEvents(String loginToken) async {
    try{
      var response =
        await httpClient.get(Uri.parse(baseUrl + "api/public_events/"),
         headers: {'Authorization' : "Bearer $loginToken"},).timeout(const Duration(seconds: 4));
    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body)["results"];
      List<Event> listEvent =
          jsonResponse.map((item) => Event.fromJson(item)).toList();
      print(listEvent);
      return listEvent;
    } else {
      print("response.statusCode ${response.statusCode}");
    }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    }
  }
  getAllEventswithPage(String loginToken, int pageNumber, int city) async {
    try{
      String url =  city == 0?"api/public_events/?page_number=$pageNumber": "api/public_events/?page_number=$pageNumber&city=$city";
       
      var response =
        await httpClient.get(Uri.parse(baseUrl + url),
         headers: {'Authorization' : "Bearer $loginToken"},).timeout(const Duration(seconds: 4));
    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body)["results"];
      int count = json.decode(response.body)["count"];
      List<Event> listEvent =
          jsonResponse.map((item) => Event.fromJson(item)).toList();
      print(listEvent);
      return [count,listEvent];
    } else {
      print("response.statusCode ${response.statusCode}");
    }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    }
  }
  Future<List?> signIn(fbaseToken) async {
    try {
      print(fbaseToken);
      final response = await httpClient.post(
        Uri.parse(baseUrl + 'api-firebase-auth/'),
        headers: {'Accept': 'application/json', 'JWT': fbaseToken},
      ).timeout(const Duration(seconds: 4));
      var data = json.decode(utf8.decode(response.bodyBytes));
      /* var data = json.decode(utf8.decode(response.bodyBytes)); */
      if (response.statusCode == 201) {
       return [true, data["token"]];
      } else {
        Get.snackbar('Error', 'Login Failed',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
      
       return [false, null];
      }
    } on TimeoutException catch (_) {
      SnackBarMessage().message('Error', 'TimeOut Error');
       return [false, null];
    } on SocketException catch (_) {
      SnackBarMessage().message('Error', 'Connection Error');
       return [false, null];
    }
    on Exception catch(_){
      SnackBarMessage().message('Error', 'Connection Error');
      return [false, null];
    }
  }

  Future<List?> signUp(User user) async {
    try {
    var fbaseToken = await user.getIdToken();
    var response = await httpClient.post(
      Uri.parse(baseUrl + 'api-firebase-register/'),
      headers: {'Accept': 'application/json', 'JWT': fbaseToken},
    ).timeout(const Duration(seconds: 4));
    var data = json.decode(utf8.decode(response.bodyBytes));
    if (response.statusCode == 201) {
       return [true, data["token"]];
    } else {
      Get.snackbar('Error', 'Login Failed',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
      
       return [false, null];
    }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
    }
  }
//TODO: store token
   Future<ParticipantData?> getParticipantData(String loginToken) async {
    var response =
        await httpClient.get(Uri.parse(baseUrl + "api/participant_data/"),
        headers: {'Authorization' : "Bearer $loginToken"}
        );
    if (response.statusCode == 200) {
      ParticipantData participantData = ParticipantData.fromJson(json.decode(response.body));
      print(participantData.vouchers);
      return participantData;
    } else {
      print("response.statusCode ${response.statusCode}");
      return null;
    }
  }

 getSponsorByVoucherID(int voucherID, String loginToken) async {
    try{
      var response =
        await httpClient.get(Uri.parse(baseUrl + "api/sponsors/$voucherID"),
         headers: {'Authorization' : "Bearer $loginToken"},).timeout(const Duration(seconds: 4));
    if (response.statusCode == 200) {
      var data = json.decode(utf8.decode(response.bodyBytes));
      print(data);
      Sponsor sponsor = Sponsor.fromJson(data);
      return [true, sponsor];
    } else {
      print("response.statusCode ${response.statusCode}");
    }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    }
  }


 getCities(String loginToken) async {
    try{
      var response =
        await httpClient.get(Uri.parse(baseUrl + "api/cities/"),
         headers: {'Authorization' : "Bearer $loginToken"},).timeout(const Duration(seconds: 4));
    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      List<City> listCity =
          jsonResponse.map((item) => City.fromJson(item)).toList();
      print(listCity);
      return [true, listCity];
    } else {
      print("response.statusCode ${response.statusCode}");
    }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return [false, null];
    }
  }
    Future logOut(loginToken) async {
    var response = await httpClient.post(
      Uri.parse("$baseUrl/auth/logout/"),
      headers: {
        'Authorization': "Bearer $loginToken",
        "Content-Type": "application/json"
      },
    ).timeout(const Duration(seconds: 4));
    if (response.statusCode == 201) {
      Get.off(LoginPgae());
    }
  }
}


