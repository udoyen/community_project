import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Model/participant_data.dart';
import 'package:voulneer_app/Views/login_page.dart';
import 'package:voulneer_app/Views/sponsot_info_page.dart';
import 'package:voulneer_app/Views/vocuher_qrcode.dart';
import 'package:voulneer_app/sizeconfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Views/vocuher_qrcode.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/sizeconfig.dart';
import 'package:voulneer_app/utils/authentication.dart';
import 'package:voulneer_app/utils/text_format.dart';

class VoucherListPage extends StatelessWidget {
  VoucherListPage({Key? key}) : super(key: key);
  final ParticipantController participantController = Get.find();

  @override
  Widget build(BuildContext context) {
    participantController.getParticipantData();
    return Scaffold(
        backgroundColor: scaffoldColor,
        appBar: AppBar(
      automaticallyImplyLeading: false,
      centerTitle: false,
      elevation: 0,
      //automaticallyImplyLeading: true,
      foregroundColor: appBarFGColor,
      backgroundColor: appBarBGColor,
            actions: [
        IconButton(onPressed: (){
          FirebaseLogin().signOutGoogle();
          participantController.logOut();
          Get.off(LoginPgae());
        }, icon: Icon(Icons.logout, color: Colors.black,))
      ],
    ),
        body: RefreshIndicator(
          onRefresh: () => participantController.getParticipantData(),
          child: Obx(() {
            if (participantController.isLoading.value &&
                (participantController.participantData.value == null)) {
              return Center(child: CircularProgressIndicator());
            } else {
              return Builder(builder: (context) {
                return Stack(
                  children: [
                    Column(
                      children: [
                        Flexible(
                            flex: 1,
                            child: Container(
                              alignment: Alignment.topLeft,
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15, 0, 10, 0),
                                child: Text(
                                  "Vouchers",
                                  style: TextStyle(
                                      fontSize: getProportionateScreenHeight(
                                          26) /* 4 * SizeConfig.heightMultiplier */,
                                      fontWeight: FontWeight.w900),
                                ),
                              ),
                            )),
                        Flexible(
                          flex: 11,
                          child: Container(
                            child: participantController
                                    .participantData.value!.vouchers.isEmpty
                                ? Center(child: Text("Empty"))
                                : ListView.builder(
                                    itemCount: participantController
                                        .participantData.value!.vouchers.length,
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                        onTap: (){
                                          var voucher = participantController.participantData.value!.vouchers[index];
                                      participantController.encodeVoucherData(voucher.id, voucher.token, voucher.voucherName);
                                      participantController.getSponsorByVoucherID(voucher.voucherId);
                                      Get.to(SponsorINFOPage(voucherID: voucher.id,));
                                        },
                                        child: Container(
                                          margin:
                                              EdgeInsets.fromLTRB(15, 3, 15, 3),
                                          decoration: BoxDecoration(
                                            color: Color(0xFFF5F6F9),
                                            shape: BoxShape.rectangle,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child: ListTile(
                                            trailing: AvailableWidget(voucher: participantController.participantData.value!.vouchers[index],),
                                              leading: SizedBox(
                                                width:
                                                    SizeConfig.widthMultiplier *
                                                        5,
                                                height:
                                                    SizeConfig.heightMultiplier *
                                                        5,
                                                child: SvgPicture.asset(
                                                  "assets/icons/coupon.svg",
                                                  color: Theme.of(context)
                                                      .primaryColor,
                                                ),
                                              ),
                                              title: Padding(
                                                padding:
                                                    const EdgeInsets.fromLTRB(
                                                        8.0, 8, 8, 0),
                                                child: Text(
                                                  participantController
                                                      .participantData
                                                      .value!
                                                      .vouchers[index]
                                                      .voucherName
                                                      .capitalizeFirstofEach,
                                                  style: TextStyle(
                                                      fontSize:
                                                          getProportionateScreenHeight(
                                                              18),
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                              subtitle: Row(
                                                children: [
                                                  Chip(
                                                      backgroundColor:
                                                          Theme.of(context)
                                                              .primaryColor
                                                              .withOpacity(0.3),
                                                      label: Text(
                                                          "${participantController.participantData.value!.vouchers[index].sponsor}",
                                                          style: TextStyle(
                                                              fontSize:
                                                                  getProportionateScreenHeight(
                                                                      18),
                                                              color: Colors.black,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500))),
                                                ],
                                              )),
                                        ),
                                      );
                                    }),
                          ),
                        ),
                      ],
                    ),
                  ],
                );
              });
            }
          }),
        ));
  }
}

class AvailableWidget extends StatelessWidget {
  const AvailableWidget({
    Key? key,
    required this.voucher,
  }) : super(key: key);

  final Vouchers voucher;

  @override
  Widget build(BuildContext context) {
    return voucher.granted ?  CircleAvatar(child: Text("U",style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),backgroundColor: Color(0xFFFF7643)) :CircleAvatar(child: Text("A",style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),backgroundColor: Colors.teal[300]);
  }
}

class VoucherListPage2 extends StatelessWidget {
  VoucherListPage2({Key? key}) : super(key: key);
  final ParticipantController participantController = Get.find();

  @override
  Widget build(BuildContext context) {
  participantController.getParticipantData();
    return Scaffold(
      backgroundColor: Color(0xffeaf2fb),
      appBar: AppBar(
                      toolbarHeight:MediaQuery.of(context).size.height/8,
                      foregroundColor: Color(0xFF3a7ed2),
                      centerTitle: true,
                      title: Text("Voulenteer", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
                      backgroundColor: Color(0xffeaf2fb),
                    ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            Flexible(child: Container(), flex: 1,),
             Container(alignment: Alignment.centerLeft,
               child: Text("Voucher List", style: TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier,fontWeight: FontWeight.bold),)),
               SizedBox(height: 3 * SizeConfig.heightMultiplier,),
            Flexible( flex:9,
              child: Container(
                child: Obx((){
                  //TODO: checl rx value 
                  print(participantController.participantData.value);
                  return (participantController.isLoading.value || (participantController.participantData.value == null))?
                  Center(child: CircularProgressIndicator()):ListView.builder( itemCount: participantController.participantData.value!.vouchers.length,
            
                    itemBuilder: (context, index){
                      return Container(
                        margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                        color: Color(0xFFFFA15C),
                        child: Padding(

                          padding: const EdgeInsets.all(2.0),
                          child: Card(
                            child: ListTile(
                              onTap: (){
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context){
                                      var voucher = participantController.participantData.value!.vouchers[index];
                                      participantController.encodeVoucherData(voucher.id, voucher.token, voucher.voucherName);
                                      return VoucherQRCode();
                                    }
                                        
                                  )
                                );
                              },
                              leading: Icon(Icons.attach_money_rounded),
                              title: Text(participantController.participantData.value!.vouchers[index].voucherName, style: TextStyle(fontSize: 2 * SizeConfig.textMultiplier,fontWeight: FontWeight.bold)),
                              subtitle: Text(participantController.participantData.value!.vouchers[index].sponsor, style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier,fontWeight: FontWeight.w200)),
                              trailing: Text(participantController.participantData.value!.vouchers[index].granted?"Used":"Available", style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier,fontWeight: FontWeight.bold) 
                              ),
                                        
                            ),
                          ),
                        ),
                      );
                    });
            
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}