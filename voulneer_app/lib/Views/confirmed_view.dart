import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Views/event_list_page.dart';
import 'package:voulneer_app/Views/home_page.dart';
import 'package:voulneer_app/sizeconfig.dart';

class ConfirmedPage extends StatefulWidget {
  ConfirmedPage({Key? key }) : super(key: key);

  @override
  _ConfirmedPageState createState() => _ConfirmedPageState();
}

class _ConfirmedPageState extends State<ConfirmedPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(child: Text("BACK"),
        backgroundColor: Colors.green,
        onPressed: () => Get.to(HomePage()),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,

          children: [
            Icon(Icons.done, size: 100, color: Colors.green,),
            SizedBox(height: 5.9 * SizeConfig.heightMultiplier,),
             Text("Done:", style: TextStyle(fontSize: 25, color: Colors.grey[800])),],
        ),
      ),
    );
  }
}