/* import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Model/event.dart';
import 'package:voulneer_app/Views/event_details.dart';
import 'package:intl/intl.dart';
import 'package:voulneer_app/utils/date_format.dart';
class EventListPage extends StatefulWidget {
  EventListPage({Key? key}) : super(key: key);

  @override
  State<EventListPage> createState() => _EventListPageState();
}

class _EventListPageState extends State<EventListPage> {
  final ParticipantController participantController =
      Get.put(ParticipantController());

@override
void initState() {
  super.initState();
  
    
}
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        body: SafeArea(
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Stack(
           children: [
              Container(
                height: MediaQuery.of(context).size.height*0.15,
                  alignment: Alignment.bottomLeft,
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Event List",
                      style: TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold),
                    ),
                  )),
              DraggableScrollableSheet(
                initialChildSize: 0.8,
                minChildSize: 0.8,
                maxChildSize: 1.0,
                builder: (context, _scrollController) {
                  // ignore: avoid_single_cascade_in_expression_statements
                  _scrollController
                    ..addListener(() {
                      if (_scrollController.position.pixels ==
                          _scrollController.position.maxScrollExtent) {
                            if (participantController.pageNumber < participantController.count){
                              participantController.pageNumber++;
                              participantController.getAllEventswithPage(0);
                            }
                      }
                    });
                  return Obx(() {
                    if (participantController.isLoading.value &&
                        participantController.events.isEmpty){
                      return const Center(child: CircularProgressIndicator());
                        } else {
                      return ListView.builder(
                        controller: _scrollController,
                         scrollDirection: Axis.vertical,
                         shrinkWrap: true,
                          itemCount: participantController.events.length,
                          itemBuilder: (context, index) {
                            return eventCard(
                                participantController.events[index], context);
                          });
                  }
                  });
                }
              )
            ]),
        ),
      )),
    );

  }
}

Widget eventCard(Event event, context) {
  var now = DateTime.now();
  ParticipantController participantController = Get.find();
  return InkWell(
    onTap: () {
      Get.to(EventDetails2(event: event));
    },
    child: Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
        color: const Color(0xFF44D492),
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: Card(
            //margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50), bottomLeft: Radius.circular(0)),
            ),
            clipBehavior: Clip.hardEdge,
            child: Padding(
              padding: const EdgeInsets.fromLTRB( 25.0, 25, 25, 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Column(
                      children: [
                        Text(event.name,
                            style:
                                const TextStyle(fontSize: 2 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold)),
                        Text(
                          now.compareTo(event.endTime) > 0 ? 'Done' : 'Active',
                          style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier,color: Colors.black.withOpacity(0.6)),
                        ),
                      ],
                    ),
                  ),

                  // ignore: prefer_const_constructors
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            event.location,
                            maxLines: 2,
                            style: const TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier),
                            overflow: TextOverflow.ellipsis,
                          ),
                          SizedBox(
                            height: 1.5 * SizeConfig.heightMultiplier,
                          ),
                          Text(
                            event.text,
                            maxLines: 2,
                            style: const TextStyle(fontSize: 13),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                      const SizedBox(height: 15),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                              DateFormat(DateYearFormat().currentFormat(event.startTime)).format(event.startTime),
                              style: const TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier)),
                          Text(" - ", style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier, color: Colors.black.withOpacity(0.8))),
                          Text(
                              DateFormat(DateYearFormat().currentFormat(event.endTime)).format(event.endTime),
                              style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier, color: Colors.black.withOpacity(0.8)))
                        ],
                      ),
                      const SizedBox(height: 1.5 * SizeConfig.heightMultiplier,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(event.organizer,
                          style: const TextStyle(color: Color(0xff230f00),)),
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor:  event.joined ? MaterialStateProperty.all(Colors.grey) : MaterialStateProperty.all(Color(0xFFFFA15C)),),
                            onPressed: event.joined
                                ? null
                                : () async {
                                    String message = await participantController
                                        .joinEvent(event.id);
                                        if (message != 'Failed') {
                                          Get.defaultDialog(title: 'Result', middleText:"", textCancel:"OK",);
                                          participantController.pageNumber = 1;
                                          participantController.getAllEventswithPage(0);
                                          
                                        }
                                        

                                  },
                            child: event.joined ? const Text("Joined", style: TextStyle(color:Colors.black),) : Text("JOIN", style: TextStyle(color:Colors.black)))

                      ],),
                      SizedBox(
                        height: 0.7 * SizeConfig.heightMultiplier,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            event.participated
                                ? const Icon(Icons.check_circle,
                                    size: 20, color: Colors.green)
                                : Container(),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
 */