import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Views/event_list_screen.dart';
import 'package:voulneer_app/Views/login_page.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/sizeconfig.dart';
import 'package:voulneer_app/utils/authentication.dart';

Tab newTab(text) => Tab(
      child: SizedBox(
        width: SizeConfig.widthMultiplier * 25,
        child: Center(
            child: Text(
          text.toUpperCase(),
          style: TextStyle(
              color: Colors.black,
              fontSize: 2.5 * SizeConfig.heightMultiplier,
              fontWeight: FontWeight.w400),
        )),
      ),
    );
Widget newTabView(int id) => EventListScreen(city: id);

class TabPage extends StatefulWidget {
  TabPage({Key? key}) : super(key: key);

  @override
  _TabPageState createState() => _TabPageState();
}

class _TabPageState extends State<TabPage> {
  ParticipantController participantController = Get.find();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ParticipantController>(
        init: Get.put(ParticipantController()),
        builder: (controller) {
          if (controller.cities.isEmpty) {
            return Scaffold(
                body: const Center(child: CircularProgressIndicator()));
          } else {
            List<Tab> tabs = [
              newTab("ALL"),
            ];
            List<Widget> tabViewList = [newTabView(0)];
            tabs.addAll(controller.cities
                .map((item) => newTab(item.city))
                .toList() as List<Tab>);
            tabViewList.addAll(
                controller.cities.map((item) => newTabView(item.id)).toList());
            return DefaultTabController(
              length: tabs.length,
              child: Scaffold(
                  backgroundColor: scaffoldColor,
                  appBar: AppBar(
                    centerTitle: false,
                    elevation: 0,
                    automaticallyImplyLeading: false,
                    foregroundColor: appBarFGColor,
                    backgroundColor: appBarBGColor,
                    actions: [
                      IconButton(
                          onPressed: () {
                            FirebaseLogin().signOutGoogle();
          participantController.logOut();
          Get.off(LoginPgae());

                          },
                          icon: Icon(
                            Icons.logout,
                            color: Colors.black,
                          ))
                    ],
                  
                  toolbarHeight: SizeConfig.heightMultiplier * 13,
                  bottom: PreferredSize(
                    preferredSize: const Size.fromHeight(20.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: TabBar(
                        tabs: tabs,
                        isScrollable: true,
                        indicatorSize: TabBarIndicatorSize.label,
                      ),
                    ),
                  ),),
                  body: TabBarView(children: tabViewList)),
            );
          }
        });
  }
}
