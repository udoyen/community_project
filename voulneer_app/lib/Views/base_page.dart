import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Views/login_page.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/utils/authentication.dart';



class BasePage extends StatefulWidget {
  BasePage({Key? key}) : super(key: key);

  @override
  BasePageState createState() => BasePageState();
}

class BasePageState<T extends BasePage> extends State<T> {
  String PageTitle ="";
  bool showLeading = true;
ParticipantController participantController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldColor,
      /* floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: floatingButton(), */
      
      appBar:_BuildAppBar(),
      body: pageUI(),
      
    );
  }
  Widget? floatingButton(){
    return null;
  }
  PreferredSizeWidget _BuildAppBar(){
    return AppBar(
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: Text(PageTitle),
      elevation: 0,
      //automaticallyImplyLeading: true,
      foregroundColor: appBarFGColor,
      backgroundColor: appBarBGColor,
      leading: showLeading?IconButton(
            onPressed: () => Get.back(),
            icon: Icon(Icons.arrow_back_ios_new_rounded),
            color: appBarFGColor,
          ):null,
      actions: [
        IconButton(onPressed: (){
          FirebaseLogin().signOutGoogle();
          participantController.logOut();
          Get.off(LoginPgae());
        }, icon: Icon(Icons.logout, color: Colors.black,))
      ],
    );

  }
  Widget pageUI(){
    return Container();
  }
}