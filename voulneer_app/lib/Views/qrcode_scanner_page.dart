import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'dart:convert';

import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Views/confirmed_view.dart';
import 'package:voulneer_app/constants.dart';



class QRScanner extends StatefulWidget {

   QRScanner({
    Key? key
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QRScannerState();
}

class _QRScannerState extends State<QRScanner> {
  ParticipantController participantController = Get.find();
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;
  bool isPressed = false;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    return SafeArea(
      child: Scaffold(
        backgroundColor: scaffoldColor,
        appBar: AppBar(
        elevation: 0,
       backgroundColor: Colors.transparent,
       leading: IconButton(onPressed: ()=>Get.back(), icon: Icon(Icons.arrow_back_ios_new_rounded), color: Color(0xff44D492),),
      ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: QRView(
                key: qrKey,
                onQRViewCreated: _onQRViewCreated,
                overlay: QrScannerOverlayShape(
                    borderColor: Colors.red,
                    borderRadius: 10,
                    borderLength: 30,
                    borderWidth: 10,
                    cutOutSize: scanArea),
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Center(
                    child: StreamBuilder(
                        stream: controller?.scannedDataStream,
                        builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) {
                          if (!snapshot.hasData) {
                            return CircularProgressIndicator();
                          }
    
                          if (snapshot.connectionState == ConnectionState.done) {}
                          try {
                            var eventUUID = snapshot.data.code;
    
                            return Container(
                                child: Center(
                              child: Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Column(
                                    children: [
                                      Card(
                                        child: ListTile(
                                          leading: Icon(Icons.domain),
                                          title: Text("Scanned"),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            RaisedButton(
                                                child: Text('Back'),
                                                onPressed: () {
                                                  Get.back();
                                                }),
                                            _continue(eventUUID),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ));
                          } on FormatException catch (e) {
                            return Text("error");
                          }
                        }),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _continue(eventUUID) {

    return RaisedButton.icon(
      onPressed: () async {
        if (!isPressed) {
          setState(() {
            isPressed = true;
          });
          String message = await participantController.participateEvent(eventUUID);
          controller!.stopCamera();
          if (message != 'Failed')
          Get.to(ConfirmedPage());
        };
          /* print(resp);
          if (resp[0]){
            Get.to(VoucherStatusPage(data:resp[1]));
          } else{
            Get.to(ScanVoucherPage());

          }

        } */
      },
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      label: !isPressed
          ? Text(
              'Continue',
              style: TextStyle(color: Colors.white),
            )
          : Text(
              'Progress ...',
              style: TextStyle(color: Colors.white),
            ),
      icon:Icon(
        Icons.arrow_forward_ios_outlined,
        color: Colors.white,
      ),
      textColor: Colors.white,
      splashColor: Colors.red,
      color: !isPressed
          ?Colors.green:Colors.grey ,
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
  }

  @override
  void dispose() {
    print("Disposing first route");
    controller?.dispose();
    super.dispose();
  }

  Map<String, dynamic> jsondecoder(jsonString) {
    return jsonDecode(jsonString);
  }
}
