import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Provider/api_provider.dart';
import 'package:voulneer_app/Views/event_list_page.dart';
import 'package:voulneer_app/Views/home_page.dart';
import 'package:voulneer_app/Views/tab_screen.dart';
import 'package:voulneer_app/Views/voucher_list_page.dart';
import 'package:voulneer_app/main.dart';
import 'package:voulneer_app/sizeconfig.dart';
import 'package:voulneer_app/utils/authentication.dart';
import 'package:http/http.dart' as http;

class LoginPgae extends StatefulWidget {
  LoginPgae({Key? key}) : super(key: key);

  @override
  _LoginPgaeState createState() => _LoginPgaeState();
}

class _LoginPgaeState extends State<LoginPgae> {
  
  final ParticipantController participantController = Get.put(ParticipantController());
  late FirebaseMessaging messaging;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value){
        participantController.FCMToken=value!;
    });
        FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message recieved");
      print(event.notification!.body);
      Get.snackbar("Notification", event.notification!.body!);

    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 29 * SizeConfig.heightMultiplier,
              width: 200,
              child: Image.network("https://image.freepik.com/free-vector/social-team-helping-charity-sharing-hope_74855-6660.jpg"),
            ),
            SizedBox(
              height: 10 * SizeConfig.heightMultiplier,
            ),
            SizedBox(
              height: 5.9 * SizeConfig.heightMultiplier,
              width: 250,
              child: ElevatedButton(
                style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Color(0xff44D492)),),
                  onPressed: () {
                    FirebaseLogin().signInWithGoogle().then((result) {
                      if (result != null) {
                        result.getIdToken().then((value) {
                          ApiProvider(
                            httpClient: http.Client(),
                          ).signIn(value).then((resp) {
                            print(resp);
                            if (resp![0] == true) {
                              participantController.loginToken = resp[1];
                              participantController.init();
                              Get.off(HomePage());
                            } else {}
                          });
                        });
                      }
                    });
                  },
                  child: Text("LOGIN with Google")),
            ),
            SizedBox(
              height: 5.9 * SizeConfig.heightMultiplier,
            ),
            SizedBox(
              height: 5.9 * SizeConfig.heightMultiplier,
              width: 250,
              child: ElevatedButton(
                style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Color(0xFFFFA15C)),),
                child: Text("REGISTER with Google"),
                onPressed: () {
                  FirebaseLogin().signInWithGoogle().then((result) {
                    if (result != null) {
                      ApiProvider(
                        httpClient: http.Client(),
                      ).signUp(result).then((resp) {

                        if (resp![0] == true) {
                          participantController.loginToken = resp[1];
                              participantController.init();
                              Get.to(HomePage());
                        } else {}
                      });
                    }
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
