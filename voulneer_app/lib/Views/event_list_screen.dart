import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Model/event.dart';
import 'package:voulneer_app/Views/event_details.dart';
import 'package:intl/intl.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/sizeconfig.dart';
import 'package:voulneer_app/utils/date_format.dart';


double totalWidth=0;
double totalHeight=0;
class EventListScreen extends StatefulWidget {
  final int city;

  EventListScreen({Key? key, required this.city}) : super(key: key);

  @override
  State<EventListScreen> createState() => _EventListScreenState();
}

class _EventListScreenState extends State<EventListScreen> {
   ParticipantController participantController =
      Get.find();

@override
void initState() {
  super.initState();
  participantController.initVars(widget.city);
participantController.getAllEventswithPage(widget.city);
  
    
}
  @override
Widget build(BuildContext context) {
  totalWidth = MediaQuery.of(context).size.width;
  totalHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      
      backgroundColor: scaffoldColor,
        body: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Stack(
             children: [
                Container(
                  height: SizeConfig.heightMultiplier * 8.33,
                    alignment: Alignment.bottomLeft,
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Events",
                        style: TextStyle(fontSize: getProportionateScreenHeight(26), fontWeight: FontWeight.w900),
                      ),
                    )),
                DraggableScrollableSheet(
                  initialChildSize: 0.9,
                  minChildSize: 0.8,
                  maxChildSize: 1.0,
                  builder: (context, _scrollController) {
                    // ignore: avoid_single_cascade_in_expression_statements
                    _scrollController
                      ..addListener(() {
                        if (_scrollController.position.pixels ==
                            _scrollController.position.maxScrollExtent) {
                              if (participantController.pageNumber[widget.city]! < (participantController.count[widget.city]!/4)){
                                participantController.pageNumber[widget.city] = participantController.pageNumber[widget.city]! + 1;
                                participantController.getAllEventswithPage(widget.city);
                              }
                        }
                      });
                    return GetBuilder<ParticipantController>(
                      builder: (controller) {
                      if (participantController.mapEvents[widget.city]!.isEmpty){
                        return const Center(child: CircularProgressIndicator());
                          } else {
                        return Container(
                          color: scaffoldColor,
                          child: ListView.builder(
                            controller: _scrollController,
                             scrollDirection: Axis.vertical,
                             shrinkWrap: true,
                              itemCount: participantController.mapEvents[widget.city]!.length,
                              itemBuilder: (context, index) {
                                return eventCard(
                                    participantController.mapEvents[widget.city]![index], context, widget.city, _scrollController);
                              }),
                        );
                    }
                    });
                  }
                )
              ]),
          ),
        ),
    );

  }
}

Widget eventCard(Event event, context, int city, ScrollController _scrollController) {
  var now = DateTime.now();
  ParticipantController participantController = Get.find();
  return InkWell(
    onTap: () {
      Get.to(EventDetails2(event: event));
    },
    child: Padding(
      padding: const EdgeInsets.fromLTRB(0,0,0,2),
      child: Container(
        color: Theme.of(context).primaryColor,
        
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: Container(
            
            //margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50), bottomLeft: Radius.circular(0)),
            ),
            //clipBehavior: Clip.hardEdge,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                event.imageFile==""?Container():
                       Container( height: 29 * SizeConfig.heightMultiplier, width: double.infinity,

                         decoration: BoxDecoration(
                           borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(50), bottomLeft: Radius.circular(0)),
                           
                         image: DecorationImage(fit:BoxFit.fill, image: NetworkImage('http://192.168.1.103/' + event.imageFile),)) 
                         ,),
                Padding(
                  padding: const EdgeInsets.fromLTRB( 30.0,30, 30, 0),
                  child: SizedBox(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        
                        Text(event.name,
                            style:
                                 TextStyle(fontSize: 2.6 * SizeConfig.textMultiplier, fontWeight: FontWeight.bold, color: Colors.black87)),
                        Text(
                          now.compareTo(event.endTime) > 0 ? 'Done' : 'Active',
                          style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier,color: Colors.black.withOpacity(0.6)),
                        ),
                      ],
                    ),
                  ),
                ),

                // ignore: prefer_const_constructors
                Padding(
                  padding: const EdgeInsets.fromLTRB( 30.0,5, 30, 5),
                  child: SizedBox(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 1.5 * SizeConfig.heightMultiplier,
                        ),
                        Text(
                          event.text,
                          maxLines: 2,
                          softWrap: true,
                          style: const TextStyle(fontSize: 16,  color: Colors.black54),
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(height: 15),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('Begin: '+
                                DateFormat(DateYearFormat().currentFormat(event.startTime)).format(event.startTime),
                                style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier, color: Colors.black,)),
                            /* Text(" - ", style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier, color: Colors.black.withOpacity(0.8))),
                            Text(
                                DateFormat(DateYearFormat().currentFormat(event.endTime)).format(event.endTime),
                                style: TextStyle(fontSize: 1.755 * SizeConfig.textMultiplier, color: Colors.black.withOpacity(0.8))) */
                          ],
                        ),
                         SizedBox(height: 1.5 * SizeConfig.heightMultiplier,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Chip(
                                backgroundColor: Color(0xFFFF7643).withOpacity(0.5),
                                /* avatar: CircleAvatar() */
                                label: Text(event.organizer,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w500)),
                              ),
                            ),
                            SizedBox(width: 20,),
                          SizedBox(
                            width: totalWidth/4,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor:  event.joined ? MaterialStateProperty.all(Colors.grey) : MaterialStateProperty.all(Theme.of(context).primaryColor),),
                                onPressed: event.joined
                                    ? null
                                    : () async {
                                        String message = await participantController
                                            .joinEvent(event.id);
                                            if (message != 'Failed') {
                                              Get.defaultDialog(title: 'Result', middleText:message, textCancel:"OK",);
                                              participantController.pageNumber[city]= 1;
                                              participantController.getAllEventswithPage(city);
                                              _scrollController.jumpTo(0);
                                              
                                            }
                                            

                                      },
                                child: event.joined ? const Text("Joined", style: TextStyle(color:Colors.black),) : Text("JOIN", style: TextStyle(color: Colors.white, fontSize: 2 * SizeConfig.textMultiplier, fontWeight: FontWeight.w600))),
                          )

                        ],),
                        SizedBox(
                          height: 0.7 * SizeConfig.heightMultiplier,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              event.participated
                                  ? const Icon(Icons.check_circle,
                                      size: 20, color: Colors.green)
                                  : Container(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ),
  );
}
