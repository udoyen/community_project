import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Views/base_page.dart';
import 'package:voulneer_app/Views/vocuher_qrcode.dart';
import 'package:voulneer_app/components/button_widget.dart';
import 'package:voulneer_app/sizeconfig.dart';

class SponsorINFOPage extends BasePage {
  final int voucherID;
  SponsorINFOPage({Key? key, required this.voucherID}) : super(key: key);

  @override
  _SponsorINFOPageState createState() => _SponsorINFOPageState();
}

class _SponsorINFOPageState extends BasePageState<SponsorINFOPage> {
  @override
  Widget pageUI() {
    // TODO: implement pageUI
    return Padding(
        padding: const EdgeInsets.all(32.0),
        child: GetBuilder<ParticipantController>(
          
          builder: (controller) {
            if (controller.sponsor.name == "N/A")
            return Center(child: CircularProgressIndicator(),);
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: CircleAvatar(
                        //backgroundImage: AssetImage("assets/images/null.bmp"),
                        radius: 40,
                      ),
                    ),
                    SizedBox(height: 20,),
                    Text("${controller.sponsor.name}", style: TextStyle(fontSize:18, fontWeight: FontWeight.bold,),),
                    SizedBox(height: 10,),
                    if (controller.sponsor.phone != "N/A")
                    Column(
                      children: [
                        Text(controller.sponsor.phone),
                        SizedBox(height: 10,),
                      ],
                    ),
                    if (controller.sponsor.link != "N/A")
                    Column(
                      children: [
                        Text(controller.sponsor.link),
                        SizedBox(height: 10,),
                      ],
                    ),
                    Divider(),
                    SizedBox(height: 10,),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        
                        SizedBox( width: (SizeConfig.screenWidth-64)/3,
                          child: Text("Address", style: TextStyle(fontWeight: FontWeight.bold,),)),
                        SizedBox(
                          width:  (SizeConfig.screenWidth-64)/3*2,
                          child: Text("${controller.sponsor.location}",),
                        ),
                      ],
                    ),
                    if (controller.sponsor.city != "N/A")
                    Column(
                      children: [
                        SizedBox(height: 20,),
                        
                        Row(crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                                               SizedBox( width: (SizeConfig.screenWidth-64)/3,
                              child: Text("City", style: TextStyle(fontWeight: FontWeight.bold,),)),
                            SizedBox(
                              width:  (SizeConfig.screenWidth-64)/3*2,
                              child: Text(controller.sponsor.city,),
                            )
                          ],),
                          SizedBox(height: 40,),
                      ],
                    ),
                  
                  ButtonWidget(text:"USE VOUCHER",fun: () =>Get.to(VoucherQRCode()) ),

                  ],
                )

              ],
            );
          }
        ),
      );
  }
  
}