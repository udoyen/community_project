import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Views/login_page.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/utils/authentication.dart';
class VoucherQRCode extends StatelessWidget {
  VoucherQRCode({Key? key}) : super(key: key);
  final ParticipantController participantController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      automaticallyImplyLeading: false,
      centerTitle: false,
      //title: Text(PageTitle),
      elevation: 0,
      //automaticallyImplyLeading: true,
      foregroundColor: appBarFGColor,
      backgroundColor: appBarBGColor,
      leading: IconButton(
            onPressed: () => Get.back(),
            icon: Icon(Icons.arrow_back_ios_new_rounded),
            color: appBarFGColor,
          ),
      actions: [
        IconButton(onPressed: (){
          FirebaseLogin().signOutGoogle();
          participantController.logOut();
          Get.off(LoginPgae());
        }, icon: Icon(Icons.logout, color: Colors.black,))
      ],
    ),
      body: Container(
        child: Center(
          child: QrImage(
            data: participantController.voucherToken,
            version: QrVersions.auto,
            size: 250,
            gapless: false,

          ),)
          ),
    );
  }
}