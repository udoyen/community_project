import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:intl/intl.dart';
import 'package:voulneer_app/Model/event.dart';
import 'package:voulneer_app/Views/qrcode_scanner_page.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/sizeconfig.dart';
import 'package:voulneer_app/utils/date_format.dart';
import 'package:voulneer_app/constants.dart';

double totalWidth = 0;
double totalHeight = 0;

class EventDetails2 extends StatelessWidget {
  Event event;
  EventDetails2({Key? key, required this.event}) : super(key: key);

  ParticipantController participantController = Get.find();

  @override
  Widget build(BuildContext context) {
    totalWidth = MediaQuery.of(context).size.width;
    totalHeight = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        backgroundColor: scaffoldColor,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            onPressed: () => Get.back(),
            icon: Icon(Icons.arrow_back_ios_new_rounded),
            color: Color(0xFF3a7ed2),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20, 20, 0),
          child: SingleChildScrollView(
            child: Card(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            event.participated
                                ? Icon(Icons.check_circle,
                                    size: 20, color: Colors.green)
                                : Container(),
                          ],
                        ),
                        Center(
                          child: Column(
                            children: [
                              Text(event.name,
                                  style: TextStyle(
                                      fontSize: 2.6 * SizeConfig.textMultiplier,
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold)),
                              Text(
                                DateTime.now().compareTo(event.endTime) > 0
                                    ? 'Done'
                                    : 'Active',
                                style: TextStyle(
                                    fontSize: 1.755 * SizeConfig.textMultiplier,
                                    color: Colors.black.withOpacity(0.6)),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 3 * SizeConfig.heightMultiplier,
                        ),
                        event.imageFile == ""
                            ? Container()
                            : Container(
                                height: 29 * SizeConfig.heightMultiplier,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12),
                                    image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: NetworkImage(
                                          'http://192.168.1.103/' +
                                              event.imageFile),
                                    )),
                              ),
                        SizedBox(
                          height: 3 * SizeConfig.heightMultiplier,
                        ),
                        Text(event.text,
                            style:
                                TextStyle(fontSize: 2.487 * SizeConfig.textMultiplier, color: Colors.black54)),
                        SizedBox(
                          height: 3 * SizeConfig.heightMultiplier,
                        ),
                        Chip(
                          label: Text(
                            event.location,
                            maxLines: 4,
                            style: TextStyle(fontSize: 2 * SizeConfig.textMultiplier),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        SizedBox(
                          height: 3 * SizeConfig.heightMultiplier,
                        ),
                        Chip(
                                backgroundColor: Color(0xff679ff0),
                                /* avatar: CircleAvatar() */
                                label: Text(event.organizer,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(color: Colors.white, fontWeight: FontWeight.w500)),
                              ),
                              SizedBox(
                          height: 3 * SizeConfig.heightMultiplier,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                    "${DateFormat(DateYearFormat().currentFormat(event.startTime)).format(event.startTime)}",
                                    style: TextStyle(fontSize: 2 * SizeConfig.textMultiplier)),
                              ),
                            ),
                            Text(" - ",
                                style: TextStyle(
                                    fontSize: 1.755 * SizeConfig.textMultiplier,
                                    color: Colors.black.withOpacity(0.8))),
                                    Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                    "${DateFormat(DateYearFormat().currentFormat(event.endTime)).format(event.endTime)}",
                                    style: TextStyle(fontSize: 2 * SizeConfig.textMultiplier)),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5.9 * SizeConfig.heightMultiplier,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            SizedBox(
                              width: totalWidth / 3,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: event.joined
                                        ? MaterialStateProperty.all(Colors.grey)
                                        : MaterialStateProperty.all(
                                            Color(0xFF88F7E2)),
                                  ),
                                  onPressed: event.joined
                                      ? null
                                      : () async {
                                          String message =
                                              await participantController
                                                  .joinEvent(event.id);
                                          showDialog<String>(
                                            context: context,
                                            builder: (BuildContext context) =>
                                                AlertDialog(
                                              title: const Text('Result'),
                                              content: Text(message),
                                              actions: <Widget>[
                                                TextButton(
                                                  onPressed: () =>
                                                      Navigator.pop(
                                                          context, 'OK'),
                                                  child: const Text('OK'),
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                  child: event.joined
                                      ? Text(
                                          "Joined",
                                          style: TextStyle(color: Colors.black, fontSize: 2 * SizeConfig.textMultiplier, fontWeight: FontWeight.w600),
                                        )
                                      : Text(
                                          "Join",
                                          style: TextStyle(color: Colors.black, fontSize: 2 * SizeConfig.textMultiplier, fontWeight: FontWeight.w600),
                                        )),
                            ),
                            SizedBox(
                              width: totalWidth / 3,
                              child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: event.participated
                                        ? MaterialStateProperty.all(Colors.grey)
                                        : MaterialStateProperty.all(
                                            Color(0xFFFFA15C)),
                                  ),
                                  onPressed: event.participated
                                      ? null
                                      : () async {
                                          participantController.eventId =
                                              event.id;
                                          print(
                                              'participantController.eventId ${participantController.eventId}');
                                          Get.to(QRScanner());
                                        },
                                  child: event.participated
                                      ? Text(
                                          "Participated",
                                          style: TextStyle(color: Colors.black, fontSize: 2 * SizeConfig.textMultiplier, fontWeight: FontWeight.w600),
                                        )
                                      : Text(
                                          "Participate",
                                          style: TextStyle(
                                              color: Colors.grey[800], fontSize: 2 * SizeConfig.textMultiplier, fontWeight: FontWeight.w600),
                                        )),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
