import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Views/event_list_page.dart';
import 'package:voulneer_app/Views/login_page.dart';
import 'package:voulneer_app/Views/tab_screen.dart';
import 'package:voulneer_app/Views/vocuher_qrcode.dart';
import 'package:voulneer_app/Views/voucher_list_page.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:voulneer_app/components/custom_nav_bar.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/utils/authentication.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var pages = [TabPage(), VoucherListPage()];

  int _selectedIndex = 0;
  ParticipantController participantController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
        body: Stack(children:[ pages[_selectedIndex],
      Align(
        alignment: Alignment.bottomLeft,
        child:  CustomBottomNavBar(index:_selectedIndex, fun: (val){ setState(() {
        _selectedIndex=val;
      }); }, ),
      )
      ]),
    );
  }
}


