import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Controller/event_controller.dart';
import 'package:voulneer_app/Provider/api_provider.dart';
import 'package:voulneer_app/Repository/api_repository.dart';
import 'package:voulneer_app/Views/login_page.dart';
import 'package:voulneer_app/Views/sponsot_info_page.dart';
import 'package:voulneer_app/Views/tab_screen.dart';
import 'package:voulneer_app/constants.dart';
import 'package:voulneer_app/sizeconfig.dart';
import 'package:voulneer_app/utils/authentication.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_messaging/firebase_messaging.dart';


Future<void> _messageHandler(RemoteMessage message) async {
  print('background message ${message.notification!.body}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_messageHandler);
  runApp(LayoutBuilder(
    builder: (context, constraints) {
      return OrientationBuilder(
        builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          return GetMaterialApp(
             debugShowCheckedModeBanner: false,
            title: 'Voulenteer',
            theme: ThemeData(
            scaffoldBackgroundColor: scaffoldColor,
            // Define the default brightness and colors.
            primaryColor: appBarFGColor,
            // Define the default font family.
            textTheme: GoogleFonts.latoTextTheme(
 // If this is not set, then ThemeData.light().textTheme is used.
            ),
            ),
            /* home: QRScanner(), */
            home: LoginPgae(),
          );
        }
      );
    }
  ));
}

