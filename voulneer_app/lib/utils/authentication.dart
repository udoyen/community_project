
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseLogin{

final FirebaseAuth _auth = FirebaseAuth.instance;

  GoogleSignIn googleSignIn = GoogleSignIn(
  /* scopes: [
      'https://www.googleapis.com/auth/classroom.courseworkmaterials.readonly',
      'https://www.googleapis.com/auth/classroom.courses.readonly'

    ], */
);


Future<User?> signInWithGoogle() async {

  await Firebase.initializeApp();
  var signedIn = await googleSignIn.isSignedIn();
  if (signedIn) {
    await googleSignIn.signOut();
  }

  final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
/* studentContainer.googleSignIn=googleSignIn; */
  final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount!.authentication;

  final AuthCredential credential = GoogleAuthProvider.credential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  final UserCredential authResult =
      await _auth.signInWithCredential(credential);
  final User? user = authResult.user;

  if (user != null) {
    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final User? currentUser = _auth.currentUser;
    assert(user.uid == currentUser!.uid);

    print('signInWithGoogle succeeded: $user');
    return user;
  }

  return null;
}

Future<void> signOutGoogle() async {

  await googleSignIn.signOut();

  print("User Signed Out");
}
}