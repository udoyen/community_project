import 'package:get/get.dart';

class SnackBarMessage{

  void message(title, message){
    Get.snackbar('$title', '$message',
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
  }
}