class DateYearFormat{
  String currentFormat(DateTime dateTime){
    if (dateTime.year == DateTime.now().year)
      return 'M/d hh:mm a'; 

    return 'y/M/d hh:mm a';

  }
}