class Event {
  Event({
    required this.name,
    required this.id,
    required this.location,
    required this.startTime,
    required this.endTime,
    required this.joining,
    required this.text,
    required this.organizer,
    required this.imageFile,
    required this.canceled,
    required this.joined,
    required this.participated,
  });
  late final String name;
  late final int id;
  late final String location;
  late final DateTime startTime;
  late final DateTime endTime;
  late final int joining;
  late final String text;
  late final String organizer;
  late final String imageFile;
  late final bool canceled;
  late final bool joined;
  late final bool participated;

  Event.fromJson(Map<String, dynamic> json){
    name = json['name'];
    id = json['id'];
    location = json['location'];
    startTime = DateTime.parse(json['startTime']);
    endTime = DateTime.parse(json['endTime']);
    joining = json['joining'];
    text = json['text'];
    organizer = json['organizer'];
    imageFile = json['imageFile'];
    canceled = json['canceled'];
    joined = json['joined'];
    participated = json['participated'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['location'] = location;
    _data['startTime'] = startTime;
    _data['endTime'] = endTime;
    _data['joining'] = joining;
    _data['text'] = text;
    _data['organizer'] = organizer;
    _data['imageFile'] = imageFile;
    _data['canceled'] = canceled;
    _data['joined'] = joined;
    _data['participated'] = participated;
    return _data;
  }
}