class City {
  late final int id;
  late final String city;

  City({required this.id, required this.city});

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    city = json['city'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['city'] = city;
    return data;
  }
}