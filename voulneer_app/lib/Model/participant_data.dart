class ParticipantData {
  ParticipantData({
    required this.fullName,
    required this.email,
    required this.vouchers,
    required this.events,
  });
  late final String fullName;
  late final String email;
  late final List<Vouchers> vouchers;
  late final List<Events> events;
  
  ParticipantData.fromJson(Map<String, dynamic> json){
    fullName = json['full_name'];
    email = json['email'];
    vouchers = List.from(json['vouchers']).map((e)=>Vouchers.fromJson(e)).toList();
    events = List.from(json['events']).map((e)=>Events.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['full_name'] = fullName;
    _data['email'] = email;
    _data['vouchers'] = vouchers.map((e)=>e.toJson()).toList();
    _data['events'] = events.map((e)=>e.toJson()).toList();
    return _data;
  }
}

class Vouchers {
  Vouchers({
    required this.id,
    required this.voucherId,
    required this.voucherName,
      this.token,
    required this.sponsor,
    required this.granted,
  });
  late final int id;
  late final int voucherId;
  late final String voucherName;
  late final String? token;
  late final String sponsor;
  late final bool granted;
  
  Vouchers.fromJson(Map<String, dynamic> json){
    id = json['id'];
    voucherId = json['voucher_id'];
    voucherName = json['voucher_name'];
    token = json['token'];
    sponsor = json['sponsor'];
    granted = json['granted'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['voucher_id'] = voucherId;
    _data['voucher_name'] = voucherName;
    _data['token'] = token;
    _data['sponsor'] = sponsor;
    _data['granted'] = granted;
    return _data;
  }
}

class Events {
  Events({
    required this.id,
    required this.name,
    required this.location,
    required this.startTime,
    required this.endTime,
  });
  late final int id;
  late final String name;
  late final String location;
  late final String startTime;
  late final String endTime;
  
  Events.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    location = json['location'];
    startTime = json['startTime'];
    endTime = json['endTime'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['location'] = location;
    _data['startTime'] = startTime;
    _data['endTime'] = endTime;
    return _data;
  }
}