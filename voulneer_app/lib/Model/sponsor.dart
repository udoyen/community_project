class Sponsor {
  Sponsor({
    required this.name,
    required this.location,
    required this.phone,
    required this.link,
    required this.city,
  });
  late final String name;
  late final String location;
  late final String phone;
  late final String link;
  late final String city;
  
  Sponsor.fromJson(Map<String, dynamic> json){
    name = json['name'];
    location = json['location'];
    phone = json['phone'];
    link = json['link'];
    city = json['city'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['location'] = location;
    _data['phone'] = phone;
    _data['link'] = link;
    _data['city'] = city;
    return _data;
  }
}