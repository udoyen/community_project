import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/constants.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/views/login_view.dart';
import 'package:sponsor_app/views/success.dart';
import 'package:sponsor_app/views/voucher_edit_page.dart';

Future<void> main() async {
    WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  runApp(LayoutBuilder(
    builder: (context, constraints) {
      return OrientationBuilder(
        builder: (context, orientation) {
          SizeConfig().init(constraints, orientation);
          return GetMaterialApp(
            debugShowCheckedModeBanner: false,
              title: 'Sponsor App',
              theme: ThemeData(
                primaryColor: appBarFGColor,
                

              ),
              home: LoginView(),
            );
        }
      );
    }
  ));
}
