import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/constants.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/utils/authentication.dart';
import 'package:sponsor_app/views/components/button.dart';
import 'package:sponsor_app/views/qrcode_scanner.dart';

class ScanVoucherPage extends StatelessWidget {
  ScanVoucherPage({Key? key}) : super(key: key);

  SposnorController sposnorController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      automaticallyImplyLeading: false,
      centerTitle: false,
      //title: Text("SCAN VOUCHER"),
      elevation: 0,
      //automaticallyImplyLeading: true,
      foregroundColor: appBarFGColor,
      backgroundColor: appBarBGColor,
      /* leading: IconButton(
            onPressed: () => Get.back(),
            icon: Icon(Icons.arrow_back_ios_new_rounded),
            color: appBarFGColor,
          ), */
      actions: [
        IconButton(onPressed: (){
          FirebaseLogin().signOutGoogle();
          sposnorController.logOut();
        }, icon: Icon(Icons.logout, color: Colors.black,))
      ],
    ),
        backgroundColor: scaffoldColor,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                Spacer(flex: 1),
                Flexible(
                    flex: 3,
                    child: ButtonWidget(
                        text: "SCAN VOUCHER", fun: () => Get.to(QRScanner()))),
                Spacer(flex: 1)
              ],
            ),
          ],
        ));
  }
}
