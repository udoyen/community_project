import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


import 'package:http/http.dart' as http;
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:sponsor_app/services/api_service.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/utils/authentication.dart';
import 'package:sponsor_app/views/components/button.dart';
import 'package:sponsor_app/views/home_page.dart';
import 'package:sponsor_app/views/qrcode_scanner.dart';
import 'package:sponsor_app/views/scan_voucher_page.dart';
import 'package:sponsor_app/views/voucher_list_page.dart';

class LoginView extends StatefulWidget {
  LoginView({Key? key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final SposnorController sponsorController = Get.put(SposnorController());
    late FirebaseMessaging messaging;
    @override
  void initState() {
    // TODO: implement initState
    
        messaging = FirebaseMessaging.instance;
    messaging.getToken().then((value){
        sponsorController.FCMToken=value!;
        print("getToken $value");
    });
        FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message recieved");
      print(event.notification!.body);
      Get.snackbar("Notification", event.notification!.body!);

    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              child: Image.network("https://picsum.photos/250?image=20"),
            ),
            SizedBox(
              height: 70,
            ),
            SizedBox(
              width: SizeConfig.screenWidth/2,
              child: ButtonWidget(text: "LOGIN with Google", fun: () {
                      FirebaseLogin().signInWithGoogle().then((result) {
                        if (result != null) {
                          result.getIdToken().then((value) {
                            ApiService().signIn(value).then((resp) {
                              print(resp);
                              if (resp![0] == true) {
                                sponsorController.loginToken = resp[1];
                                sponsorController.init();
                                Get.off(HomePage());
                              } else {
                                Get.snackbar('Error',
                                    'Failed to Login',
                                    duration: const Duration(seconds: 4),
                                    snackPosition: SnackPosition.BOTTOM,
                                    snackStyle: SnackStyle.FLOATING);
                              }
                            });
                          });
                        }
                      });
                    }),
            ),
            SizedBox(
              height: 40,
            ),

          ],
        ),
      ),
    );
  }
}
