import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/views/home_page.dart';
import 'package:sponsor_app/views/scan_voucher_page.dart';

class VoucherStatusPage extends StatefulWidget {
  Map<String, dynamic> data;
  VoucherStatusPage({Key? key, required this.data}) : super(key: key);

  @override
  _VoucherStatusPageState createState() => _VoucherStatusPageState();
}

class _VoucherStatusPageState extends State<VoucherStatusPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Text("BACK"),
        backgroundColor: Colors.green,
        onPressed: () => Get.off(HomePage()),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
              image: AssetImage('assets/images/success.gif'),
              height: 150.0,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Text(
                'Success',
                style: TextStyle(
                  fontSize: getProportionateScreenHeight(25),
                  color: Color(0xFF303030),
                ),
              ),
            ),
            SizedBox(
              height: getProportionateScreenHeight(20),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("Voucher:",
                    style: TextStyle(
                        fontSize: getProportionateScreenHeight(20),
                        color: Colors.grey[800])),
                SizedBox(
                  height: getProportionateScreenHeight(20),
                ),
                Text(widget.data['voucher'],
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: getProportionateScreenHeight(20),
                        color: Colors.blue[900])),
              ],
            ),
            SizedBox(
              height: getProportionateScreenHeight(20),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("Name:",
                    style: TextStyle(
                        fontSize: getProportionateScreenHeight(20),
                        color: Colors.grey[800])),
                SizedBox(
                  height: getProportionateScreenHeight(20),
                ),
                Text(widget.data['full_name'],
                    style: TextStyle(
                        fontSize: getProportionateScreenHeight(20),
                        color: Colors.blue[900])),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
