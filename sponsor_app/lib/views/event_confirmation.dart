import 'package:flutter/material.dart';
import 'package:sponsor_app/constants.dart';

class EventGrantConfirmation extends StatelessWidget {
  const EventGrantConfirmation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldColor,
      appBar: AppBar(
        foregroundColor: appBarFGColor,
        backgroundColor: appBarBGColor,
      ),
      body: Center(child: Text("Done")),
    );
  }
}