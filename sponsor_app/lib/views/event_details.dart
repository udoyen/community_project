import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/constants.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:sponsor_app/models/event.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:intl/intl.dart';
import 'package:sponsor_app/utils/data_format.dart';
import 'package:sponsor_app/views/components/button.dart';
import 'package:sponsor_app/views/grant_event_voucher_page.dart';


double totalWidth = 0;
double totalHeight = 0;

class EventDetails2 extends StatelessWidget {
  Event event;
  EventDetails2({Key? key, required this.event}) : super(key: key);

  SposnorController sposnorController = Get.find();

  @override
  Widget build(BuildContext context) {
    totalWidth = MediaQuery.of(context).size.width;
    totalHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: scaffoldColor,
      appBar: AppBar(
    centerTitle: false,
    //title: Text("EVENT PAGE"),
    elevation: 0,
    //automaticallyImplyLeading: true,
    foregroundColor: appBarFGColor,
    backgroundColor: appBarBGColor,
    leading: IconButton(
          onPressed: () => Get.back(),
          icon: Icon(Icons.arrow_back_ios_new_rounded),
          color: appBarFGColor,
        ),
    actions: [
      IconButton(onPressed: (){}, icon: Icon(Icons.logout, color: Colors.black,))
    ],
    ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 20, 20, 0),
        child: SingleChildScrollView(
          child: Card(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  width: double.infinity,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          event.participated
                              ? Icon(Icons.check_circle,
                                  size: 20, color: Colors.green)
                              : Container(),
                        ],
                      ),
                      Center(
                        child: Column(
                          children: [
                            Text(event.name,
                                style: TextStyle(
                                    fontSize: 2.6 * SizeConfig.textMultiplier,
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold)),
                            Text(
                              DateTime.now().compareTo(event.endTime) > 0
                                  ? 'Done'
                                  : 'Active',
                              style: TextStyle(
                                  fontSize: 1.755 * SizeConfig.textMultiplier,
                                  color: Colors.black.withOpacity(0.6)),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                    height:getProportionateScreenHeight(20),
                  ),
                      event.imageFile == ""
                          ? Container()
                          : Container(
                              height: 29 * SizeConfig.heightMultiplier,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(
                                        'http://192.168.1.103/' +
                                            event.imageFile),
                                  )),
                            ),
                      SizedBox(
                    height:getProportionateScreenHeight(20),
                  ),
                      Text(event.text,
                          style:
                              TextStyle(fontSize: 2.487 * SizeConfig.textMultiplier, color: Colors.black54)),
                      SizedBox(
                    height:getProportionateScreenHeight(10),
                  ),
                  Card(
                    child:Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                            event.location,
                            maxLines: 4,
                            style: TextStyle(fontSize: 2 * SizeConfig.textMultiplier),
                            overflow: TextOverflow.ellipsis,
                          ),
                    )
                  ),
                       SizedBox(
                    height:getProportionateScreenHeight(10),
                  ),
                      Chip(
                              backgroundColor: Color(0xFFFF7643).withOpacity(0.5),
                              /* avatar: CircleAvatar() */
                              label: Text(event.organizer,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w500)),
                            ),
                      
                            SizedBox(
                        height:getProportionateScreenHeight(10),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                  "${DateFormat(DateYearFormat().currentFormat(event.startTime)).format(event.startTime)}",
                                  style: TextStyle(fontSize: 2 * SizeConfig.textMultiplier)),
                            ),
                          ),
                          Text(" - ",
                              style: TextStyle(
                                  fontSize: 1.755 * SizeConfig.textMultiplier,
                                  color: Colors.black.withOpacity(0.8))),
                                  Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                  "${DateFormat(DateYearFormat().currentFormat(event.endTime)).format(event.endTime)}",
                                  style: TextStyle(fontSize: 2 * SizeConfig.textMultiplier)),
                            ),
                          ),
                        ],
                      ),
                      if(DateTime.now().compareTo(event.endTime) > 0)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                    height:getProportionateScreenHeight(10),
                  ),
                          Chip(
                            label: Row(
                              mainAxisSize: MainAxisSize.min,
                              children:[
                              Text("Participants: "),
                              Text("${event.participant}", style: TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),),
                              ]
                            ),
                           avatar: CircleAvatar(child: Text("P"),
                           ), 
                          ),
                                                SizedBox(
                        height:getProportionateScreenHeight(30),
                      ),

                      ButtonWidget(text: "SPONSOR", fun: () {
                              Get.to(GrantEventVoucherPage(event:event));
                            })

                        ],
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
