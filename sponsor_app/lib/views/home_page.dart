import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:sponsor_app/constants.dart';
import 'package:sponsor_app/enums.dart';
import 'package:sponsor_app/views/components/custom_nav_bar.dart';
import 'package:sponsor_app/views/scan_voucher_page.dart';
import 'package:sponsor_app/views/tab_page.dart';
import 'package:sponsor_app/views/history_voucher_event_list_page.dart';
import 'package:sponsor_app/views/voucher_list_page.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int index = 0;
  var pages = [TabPage(),VoucherListPage(),ScanVoucherPage(), HistoryVoucherEventList()];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children:[ pages[index],
      Align(
        alignment: Alignment.bottomLeft,
        child:  CustomBottomNavBar(index:index, fun: (val){ setState(() {
        index=val;
      }); }, ),
      )
      ]),

    );
  }
}
