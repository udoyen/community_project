import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:sponsor_app/models/voucher_model.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/utils/form_helper.dart';
import 'package:sponsor_app/views/base_page.dart';

class VoucherAddPage extends BasePage {
  Voucher? voucher;
  VoucherAddPage({Key? key, this.voucher}) : super(key: key);

  @override
  _VoucherAddPageState createState() => _VoucherAddPageState();
}

class _VoucherAddPageState extends BasePageState<VoucherAddPage> {
  bool isPressed = false;
  SposnorController sposnorController = Get.find();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var txtName = TextEditingController();
  var txtquantity = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.PageTitle = "ADD VOUCHER";

  }

@override
Widget pageUI(){
  return  Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        child: Form(
            key: formKey,
            child: ListView(
              children: [
                SizedBox(height:getProportionateScreenHeight(30),),
                if (widget.voucher == null)
                Column(
                  children: [
                    TextFormField(
                      validator: (value) =>
                          validateInput(value, "Voucher Name"),
                      controller: txtName,
                      decoration: fieldDecoration(
                        context: context,
                        labelText: "Voucher Name",
                      ),
                    ),
                    SizedBox(
                      height:getProportionateScreenHeight(30),
                    ),
                    TextFormField(
                      controller: txtquantity,
                    
                      validator: (value) => validateInput(value, "Quantity"),
                      keyboardType: TextInputType.number,
                      decoration: fieldDecoration(
                        context: context,
                        labelText: "Quantity",
                      ),
                    ),
                    SizedBox(
                      height:getProportionateScreenHeight(40),
                    ),
                  ],
                ),

               SizedBox(
                    width: double.infinity,
                    height: getProportionateScreenHeight(55),
                    child: ElevatedButton(
                      style: ButtonStyle(
                         backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),

                      ),
                      
                        onPressed:isPressed?null: () async {
                          if (formKey.currentState!.validate()) {

                            if (!isPressed) {
                                    isPressed = true;
                                    setState(() {
                                      
                                    });
                                    List result =
                                        await sposnorController.addVoucher(
                                            txtName.text,
                                            int.parse(txtquantity.text));
                                    if (result[0]) {
                                      
                                      Get.snackbar('Success', 'Done',
                                          snackbarStatus: (val) {
                                        if (val == SnackbarStatus.CLOSING) {
                                          setState(() {
                                            txtName.text="";
                                            txtquantity.text="";
                                            isPressed = false;
                                          });
                                        }
                                      },
                                          duration: const Duration(seconds: 3),
                                          snackPosition: SnackPosition.BOTTOM,
                                          snackStyle: SnackStyle.FLOATING);
                                          
                                    } else {
                                      Get.snackbar('Failed', 'Failed',
                                      snackbarStatus: (val) {
                                        if (val == SnackbarStatus.CLOSING) {
                                          setState(() {
                                            isPressed = false;
                                          });
                                        }
                                      },
                                          duration: const Duration(seconds: 3),
                                          snackPosition: SnackPosition.BOTTOM,
                                          snackStyle: SnackStyle.FLOATING);
                            }
                          }}

                        },
                        child: Text("ADD")))
              ],
            )),
      ),
    );
  
}

  String? validateInput(value, message) {
    if (value == null || value.isEmpty) {
      return "Please enter $message";
    }
    return null;
  }
}

InputDecoration fieldDecoration({
  required BuildContext context,
  bool filled = false,
  Color fillColor=const Color(0xffe0e0e0),
  String? labelText,
  String? hintText,
  String? helperText,
  Widget? prefixIcon,
  Widget? suffixIcon,
}) {
  return InputDecoration(
    filled: filled,
    fillColor: fillColor,
    contentPadding: EdgeInsets.all(6),
    labelText: labelText,
    hintText: hintText,
    helperText: helperText,
    prefixIcon: prefixIcon,
    suffixIcon: suffixIcon,
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Theme.of(context).primaryColor,
        width: 1,
      ),
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: Theme.of(context).primaryColor,
        width: 1,
      ),
    ),
  );
}
