import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:sponsor_app/models/event.dart';
import 'package:sponsor_app/models/voucher_model.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/views/base_page.dart';
import 'package:sponsor_app/views/components/button.dart';
import 'package:sponsor_app/views/components/snackbar.dart';
import 'package:sponsor_app/views/event_confirmation.dart';
import 'package:sponsor_app/utils/text_format.dart';
import 'package:sponsor_app/views/success.dart';
class GrantEventVoucherPage extends BasePage {
  final Event event;
  GrantEventVoucherPage({Key? key, required this.event}) : super(key: key);

  @override
  _GrantEventVoucherPageState createState() => _GrantEventVoucherPageState();
}

class _GrantEventVoucherPageState extends BasePageState<GrantEventVoucherPage> {
  SposnorController sposnorController = Get.find();
  List<int> selected = [];
  @override
  void initState() {
    super.initState();
    this.PageTitle="GRANT VOUCHER";
  }
  @override
  Widget pageUI() {
    // TODO: implement pageUI
    return Column(
        children: [
          Flexible(flex:1,child: Container(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15,0,10,0),
                            child: Text("Vouchers", style: TextStyle(fontSize: getProportionateScreenHeight(26) /* 4 * SizeConfig.heightMultiplier */, fontWeight: FontWeight.w900),),
                          ),)),
          Flexible(flex:6,
            child: ListView.builder(
              itemCount: sposnorController.vouchers.length,
              itemBuilder: (context,index){
              return Container(
                margin: EdgeInsets.fromLTRB(15, 3, 15, 3),
                child: FilterChip(
                   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  showCheckmark: false,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  label:  Row(
                    //crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                              Flexible(
                                flex: 3,
                          child: Container(
                            alignment: Alignment.centerLeft,
                            width: double.infinity,
                            child: Chip(
                              backgroundColor: Theme.of(context)
                                  .primaryColor
                                  .withOpacity(0.4),
                              avatar: SizedBox(
                                child: CircleAvatar(
                                    child: Text(
                                      "V",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    backgroundColor:
                                          Theme.of(context).primaryColor),
                              ),
                                                            label: SizedBox(
                                                              width: double.infinity,
                                                              child: Text(
                                                                                                '${sposnorController.vouchers[index].name.capitalize}',
                                                                                                overflow: TextOverflow.ellipsis,
                                                                                                maxLines: 3,
                                                                                                style: TextStyle(color: Colors.black)),
                                                            ),
                                                          ),
                                ),
                              ),
                          Flexible(
                            flex: 1,
                            child: Container(
                              alignment: Alignment.centerRight,
                              width: double.infinity,
                              child: Chip(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                  avatar: SizedBox(
                                    child: CircleAvatar(
                                        child: Text("A",
                                            style: TextStyle(color: Colors.white)),
                                        backgroundColor: Color(0xFFFF7643)),
                                  ),
                                  backgroundColor:
                                      Color(0xFFFF7643).withOpacity(0.3),
                                  label: Text(
                                      "${sposnorController.vouchers[index].available}",
                                      style: TextStyle(
                                          fontSize:
                                              getProportionateScreenHeight(18),
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500))),
                            ),
                          ),
                        ],
                      ), 
                backgroundColor: Colors.transparent,
                    selectedColor: Colors.grey[300],
                    
                    selected:
                        selected.contains(sposnorController.vouchers[index].id),
                    onSelected: widget.event.participant >
                            sposnorController.vouchers[index].available
                        ? (bool value) {
                            Get.snackbar("Error", "No Enough Vouchers",
                                snackPosition: SnackPosition.BOTTOM,
                                snackStyle: SnackStyle.FLOATING,
                                duration: const Duration(seconds: 1));
                          }
                        : (bool value) {
                            if (selected.contains(
                                sposnorController.vouchers[index].id)) {
                              selected
                                  .remove(sposnorController.vouchers[index].id);
                              print(selected);
                    }
                  else {
                    selected.add(sposnorController.vouchers[index].id);
                    print(selected);
                  }
                  setState(() {
                    
                  });
                 },),
              );
          
            }),
          ),
       Flexible(flex:2,child: Padding(
         padding: const EdgeInsets.all(10.0),
         child: ButtonWidget(text: "GRANT", fun: () async {
           if (selected.isNotEmpty ){

           var result = await sposnorController.grantVoucher(widget.event.id, selected);
           if (result){
             Get.off(Success());
           }
           } else {
             getSnackBar("Error", "Please Select Voucher");
           }
           }),
       )),
      ],
      );
    
  }

}