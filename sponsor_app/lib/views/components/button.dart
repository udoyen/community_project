import 'package:flutter/material.dart';
import 'package:sponsor_app/sizeconfig.dart';

class ButtonWidget extends StatelessWidget {
  Function()? fun;
  String text;
  ButtonWidget({Key? key, required this.text, required this.fun})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        height: getProportionateScreenHeight(55),
        child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(
                  Theme.of(context).primaryColor),
            ),
            onPressed: fun,
            child: Text(text)));
  }
}
