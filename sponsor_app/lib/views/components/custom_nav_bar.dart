import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';



import 'package:sponsor_app/enums.dart';
import 'package:sponsor_app/views/event_list_page.dart';
import 'package:sponsor_app/views/tab_page.dart';
import 'package:sponsor_app/views/voucher_list_page.dart';

class CustomBottomNavBar extends StatelessWidget {
  CustomBottomNavBar({
    Key? key,
    required this.index,  required this.fun
  }) : super(key: key);
  Function fun;
  final int index;

  @override
  Widget build(BuildContext context) {
    final Color inActiveIconColor = Color(0xFFB6B6B6);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 14),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          ),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
      ),
      child: SafeArea(
          top: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                icon: SvgPicture.asset(
                  "assets/icons/event.svg",
                  
                  color: index == 0
                      ? Colors.blue
                      : inActiveIconColor,
                ),
                onPressed: () =>
                    fun(0)
              ),
              IconButton(
                icon: SvgPicture.asset(
                  "assets/icons/voucher.svg",
                  
                  color: index == 1
                      ? Colors.blue
                      : inActiveIconColor,
                ),
                onPressed: () =>
                   fun(1)
              ),
              IconButton(
                icon: SvgPicture.asset(
                  "assets/icons/qr-scan.svg",
                  
                  color: index == 2
                      ? Colors.blue
                      : inActiveIconColor,
                ),
                onPressed: () =>
                   fun(2)
              ),
              IconButton(
                icon: SvgPicture.asset(
                  "assets/icons/history.svg",
                  color: index == 3
                      ? Colors.blue
                      : inActiveIconColor,
                ),
                onPressed: () =>
                   fun(3)
              ),
            ],
          )),
    );
  
  }
}