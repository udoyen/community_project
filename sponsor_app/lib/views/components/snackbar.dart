import 'package:flutter/material.dart';
import 'package:get/get.dart';

getSnackBar(title, message){
  return Get.snackbar(title, message,
          duration: const Duration(seconds: 3),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
}