import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:sponsor_app/models/voucher_model.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/utils/form_helper.dart';
import 'package:sponsor_app/views/base_page.dart';
import 'package:sponsor_app/views/components/button.dart';

class VoucherEditPage extends BasePage {
  Voucher? voucher;
  VoucherEditPage({Key? key, this.voucher}) : super(key: key);

  @override
  _VoucherEditPageState createState() => _VoucherEditPageState();
}

class _VoucherEditPageState extends BasePageState<VoucherEditPage> {
  bool isPressed = false;
  SposnorController sposnorController = Get.find();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  var txttotal = TextEditingController();
  var txtAvailable = TextEditingController();
  var txtquantity = TextEditingController();
  var txtAmount = TextEditingController();
  int ammount = 0, total = 0, available = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.PageTitle = "EDIT VOUCHER";
    txttotal.text = widget.voucher != null
        ? widget.voucher?.quantity.toString() as String
        : "";
    txtquantity.text = widget.voucher != null
        ? widget.voucher?.quantity.toString() as String
        : "";
    txtAvailable.text = widget.voucher != null
        ? widget.voucher?.available.toString() as String
        : "0";
        txtAmount.text = "0";
  }

refreshInputs(Voucher newVoucher){
  txtAmount.text = "";
  txtquantity.text = newVoucher.quantity.toString();
  txtAvailable.text = newVoucher.available.toString();
  txttotal.text = newVoucher.quantity.toString();
  txtAmount.text = "0";
  widget.voucher = newVoucher;

}
  updateVaribales(int newAmmount) {
    total = int.parse(txtquantity.text) + newAmmount;
    available = widget.voucher!.available + newAmmount;
    txtAvailable.text = available.toString();
    txttotal.text = total.toString();
  }

@override
Widget pageUI(){
  return  Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12) )
        ),
        
        
        child: Padding(
          padding: const EdgeInsets.fromLTRB(5,0,5,0),
          child: Form(
              key: formKey,
              child: ListView(
                children: [
                  SizedBox(height:getProportionateScreenHeight(30)),
                  
                  if (widget.voucher != null)
                    Column(
                      children: [
                        TextFormField(
                          enabled: false,
                          validator: (value) =>
                              validateInput(value, "Voucher Name"),
                          initialValue: widget.voucher?.name.toString(),
                          decoration: fieldDecoration(
                            fillColor: Color(0xffeeeeee),
                            filled: true,
                            context: context,
                            labelText: "Voucher Name",
                          ),
                        ),
                        SizedBox(height:getProportionateScreenHeight(30)),
                        TextFormField(
                          enabled: false,
                          controller: txtquantity,
                          validator: (value) => validateInput(value, "Quantity"),
                          keyboardType: TextInputType.number,
                          decoration: fieldDecoration(
                            fillColor: Color(0xffeeeeee),
                            
                            filled: true,
                            context: context,
                            labelText: "Quantity",
                          ),
                        ),
                        SizedBox(
                         height:getProportionateScreenHeight(30),
                        ),
                        TextFormField(
                          
                            controller: txtAvailable,
                            enabled: false,
                            validator: (value) =>
                                validateInput(value, "Available"),
                            keyboardType: TextInputType.number,
                            decoration: fieldDecoration(
                              fillColor: Color(0xffffeeee),
                              filled: true,
                              context: context,
                              labelText: "Available",
                            )),
                        SizedBox(
                         height:getProportionateScreenHeight(30),
                        ),
                        TextFormField(
                            onChanged: (value) => setState(() {
                                  if (value == "") value = "0";
                                  updateVaribales(int.parse(value));
                                }),
                            controller: txtAmount,
                            keyboardType: TextInputType.number,
                            decoration: fieldDecoration(
                              context: context,
                              labelText: "Amount",
                            )),
                        SizedBox(
                         height:getProportionateScreenHeight(30),
                        ),
                        TextFormField(
                          
                            controller: txttotal,
                            enabled: false,
                            keyboardType: TextInputType.number,
                            decoration: fieldDecoration(
                              fillColor: Color(0xffffeeee),
                              filled: true,
                              context: context,
                              labelText: "Total Number",
                            )),
                        SizedBox(
                          height:getProportionateScreenHeight(40),
                        ),
                      ],
                    ),
                    ButtonWidget(text: "EDIT", fun: isPressed?null: () async {
                            if (formKey.currentState!.validate()) {

                              if (!isPressed) {
                                      isPressed = true;
                                      List result =
                                          await sposnorController.editVoucher(
                                              widget.voucher!.id,
                                              int.parse(txtAmount.text));
                                      if (result[0]) {
                                        setState(() {
                                          refreshInputs(result[1]);
                                        });
                                        Get.snackbar('Success', 'Done',
                                            snackbarStatus: (val) {
                                          if (val == SnackbarStatus.CLOSING) {
                                            setState(() {
                                              isPressed = false;
                                            });
                                          }
                                        },
                                            duration: const Duration(seconds: 3),
                                            snackPosition: SnackPosition.BOTTOM,
                                            snackStyle: SnackStyle.FLOATING);
                                      } else {
                                        Get.snackbar('Failed', 'Failed',
                                            duration: const Duration(seconds: 3),
                                            snackPosition: SnackPosition.BOTTOM,
                                            snackStyle: SnackStyle.FLOATING);
                              }
                            }}}),
                ],
              )),
        ),
      ),
    );
  
}

  String? validateInput(value, message) {
    if (value == null || value.isEmpty) {
      return "Please enter $message";
    }
    return null;
  }
}

InputDecoration fieldDecoration({
  required BuildContext context,
  bool filled = false,
  Color fillColor=const Color(0xffe0e0e0),
  String? labelText,
  String? hintText,
  String? helperText,
  Widget? prefixIcon,
  Widget? suffixIcon,
}) {
  return InputDecoration(
    filled: filled,
    fillColor: fillColor,
    contentPadding: EdgeInsets.all(6),
    labelText: labelText,
    hintText: hintText,
    helperText: helperText,
    prefixIcon: prefixIcon,
    suffixIcon: suffixIcon,
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Theme.of(context).primaryColor,
        width: 1,
      ),
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: Theme.of(context).primaryColor,
        width: 1,
      ),
    ),
  );
}
