import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/constants.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/views/base_page.dart';
import 'package:sponsor_app/views/components/button.dart';
import 'package:sponsor_app/views/home_page.dart';

class Success extends BasePage {
  Success({Key? key}) : super(key: key);

  @override
  _SuccessState createState() => _SuccessState();
}

class _SuccessState extends BasePageState<Success> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage('assets/images/success.gif'),
                  height: 150.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Text(
                    'Success',
                    style: TextStyle(
                      fontSize: getProportionateScreenHeight(25),
                      color: Color(0xFF303030),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(40),
          ),
          SizedBox(
              width: SizeConfig.screenWidth / 2,
              child: ButtonWidget(
                  text: 'Return to Home', fun: () => Get.off(HomePage()))),
        ],
      ),
    );
  }
}
