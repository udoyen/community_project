import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/views/base_page.dart';
import 'package:sponsor_app/views/components/snackbar.dart';
import 'package:sponsor_app/views/voucher_add_page.dart';
import 'package:sponsor_app/views/voucher_edit_page.dart';
import 'package:sponsor_app/utils/text_format.dart';
class VoucherListPage extends BasePage {
  VoucherListPage({Key? key}) : super(key: key);

  @override
  _VoucherListPageState createState() => _VoucherListPageState();
}

class _VoucherListPageState extends BasePageState<VoucherListPage> {
  SposnorController sponsorController = Get.find();
  @override
  void initState() {
    super.initState();
    sponsorController.getVouchers();
    // this.PageTitle = "Voucher List";
    this.showLeading = false;
  }
 
  Widget floatingButton() {
    
    return FloatingActionButton(
      backgroundColor: Theme.of(context).primaryColor,
        child: Icon(Icons.add),
        onPressed: () => Get.to(VoucherAddPage())!
            .then((value) => sponsorController.getVouchers()));
  }

  @override
  Widget pageUI() {
    // TODO: implement pageUI
    return RefreshIndicator(
      onRefresh:()=> sponsorController.getVouchers(),
      child: Obx(() {
          if (sponsorController.voucherIsLoading.value && sponsorController.vouchers.isEmpty){
            return Center(child: CircularProgressIndicator());
          }else{
            return Builder(
              builder: (context) {
                return Stack(
                  children: [
                    Column(
                      children: [
                        Flexible(flex:1,child: Container(
                          alignment: Alignment.topLeft,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(15,0,10,0),
                            child: Text("Vouchers", style: TextStyle(fontSize: getProportionateScreenHeight(26) /* 4 * SizeConfig.heightMultiplier */, fontWeight: FontWeight.w900),),
                          ),)),
                        Flexible(

                          flex:11,
                          child: Container(
                            
                            child: sponsorController.vouchers.isEmpty? Center(child: Text("Empty")):ListView.builder(
                              itemCount: sponsorController.vouchers.length,
                              itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.fromLTRB(15, 3, 15, 3),
                                    decoration: BoxDecoration(
                                      color: Color(0xFFF5F6F9),
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                      
          
                                    child: Slidable(
                                    
                                    key: ValueKey(sponsorController.vouchers[index].id),
                                    startActionPane: ActionPane(
                                        motion: ScrollMotion(),
                                        children: [
                                          SlidableAction(
                                            onPressed: (context){
                                              Get.to(VoucherEditPage(voucher: sponsorController.vouchers[index]))!.then((value) => sponsorController.getVouchers());
                                            },
                                            backgroundColor: Colors.blue,
                                            foregroundColor: Colors.white,
                                            icon: Icons.edit,
                                            label: 'Edit',
                                          ),
                                        ],
                                      ),
                                      endActionPane: ActionPane(
                                       
                                        motion: const ScrollMotion(),
                                        children: [
                                          SlidableAction(
                                            onPressed: (context){
                                              if(sponsorController.vouchers[index].quantity == sponsorController.vouchers[index].available){
                                                Get.defaultDialog(
                                                  title: "Confirmation",
                                                  middleText: "Are you sure you want to delete this item?",
                                                  onConfirm: ()=> sponsorController.deleteVoucher(sponsorController.vouchers[index].id).then((value) {
                                                    Get.back();
                                                    if (value){
                                                        sponsorController.getVouchers();
                                                    }
                                                  }),

                                                );
                                              } else {
                                                getSnackBar("Error", "You Cannot Delete Vocuher You Already Used Before.");
                                              }

                                            },
                                            backgroundColor: Color(0xFFFE4A49),
                                            foregroundColor: Colors.white,
                                            icon: Icons.delete,
                                            label: 'Delete',
                                          ),
                                        ],
                                      ),
                                        child: ListTile(
                                            leading: SizedBox(
                                              width: SizeConfig.widthMultiplier*5,
                                              height: SizeConfig.heightMultiplier*5,
                                              child: SvgPicture.asset(
                                                
                                                "assets/icons/coupon.svg",
                                                color: Theme.of(context).primaryColor,
                                              ),
                                            ),
                                            title: Padding(
                                              padding: const EdgeInsets.fromLTRB(8.0,8,8,0),
                                              child: Text(
                                                sponsorController.vouchers[index].name.capitalizeFirstofEach,
                                                style: TextStyle(
                                                  fontSize: getProportionateScreenHeight(18),
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w500),
                                              ),
                                            ),
                                            subtitle: Row(
                                              children: [
                                                Chip(avatar: CircleAvatar(child: Text("Q",style: TextStyle(color: Colors.white)),backgroundColor: Theme.of(context)
                                                        .primaryColor),
                                                    backgroundColor: Theme.of(context)
                                                        .primaryColor
                                                        .withOpacity(0.3),
                                                    label: Text("${sponsorController.vouchers[index].quantity}",
                                                        
                                                        style: TextStyle(
                                                          fontSize: getProportionateScreenHeight(18),
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.w500))),
                                                SizedBox(width: 20),
                                                Chip(
                                                  avatar: CircleAvatar(child: Text("A",style: TextStyle(color: Colors.white)),backgroundColor: Color(0xFFFF7643)),
                                                    backgroundColor: Color(0xFFFF7643)
                                                        .withOpacity(0.3),
                                                    label: Text("${sponsorController.vouchers[index].available}",
                                                        style: TextStyle(
                                                          fontSize: getProportionateScreenHeight(18),
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.w500))),
                                              ],
                                            )),
                                    ),
                                  );
                                }
                              ),
                          ),
                        ),
                      ],
                    ),
                  Positioned(
                    bottom: getProportionateScreenHeight(120),
                    right:getProportionateScreenHeight(50) ,
                    child: floatingButton(),
                  )
                  ],
                );
              }
            );
        }}
        ),
    );
  }
}