import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'dart:convert';

import 'package:sponsor_app/services/api_service.dart';
import 'package:sponsor_app/views/scan_voucher_page.dart';
import 'package:sponsor_app/views/voucher_status_page.dart';


class QRScanner extends StatefulWidget {

   QRScanner({
    Key? key
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QRScannerState();
}

class _QRScannerState extends State<QRScanner> {
  SposnorController sponsorController = Get.find();
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  QRViewController? controller;
  bool isPressed = false;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      floatingActionButton: SizedBox(
        height: 50,
        width: 100,
        child: FloatingActionButton(
          backgroundColor: Theme.of(context).primaryColor,
          elevation: 2,
          shape: BeveledRectangleBorder(
            borderRadius: BorderRadius.circular(5)
     ),
          onPressed: ()=> Get.back(),
          
          child: Text("Back")),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                  borderColor: Colors.red,
                  borderRadius: 10,
                  borderLength: 30,
                  borderWidth: 10,
                  cutOutSize: scanArea),
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Center(
                  child: StreamBuilder(
                      stream: controller?.scannedDataStream,
                      builder: (BuildContext context,
                          AsyncSnapshot<dynamic> snapshot) {
                        if (!snapshot.hasData) {
                          return CircularProgressIndicator();
                        }

                        if (snapshot.connectionState == ConnectionState.done) {}
                        try {
                          print(jsonDecode(utf8.decode(base64.decode(snapshot.data.code)))["id"]);
                          var voucherData = jsonDecode(
                              utf8.decode(base64.decode(snapshot.data.code)));

                          return Container(
                              child: Center(
                            child: Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  children: [
                                    Card(
                                      child: ListTile(
                                        leading: Icon(Icons.domain),
                                        title: Text("voucher"),
                                        subtitle: Text(voucherData["voucher"]),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          RaisedButton(
                                              child: Text('Back'),
                                              onPressed: () {
                                                Get.back();
                                              }),
                                          _continue(voucherData["id"] , voucherData["token"]),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ));
                        } on FormatException catch (e) {
                          return Text("error");
                        }
                      }),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _continue(id, token) {

    return RaisedButton.icon(
      onPressed: () async {
        if (!isPressed) {
          setState(() {
            isPressed = true;
          });
          var resp= await ApiService().checkVoucher(id, token, sponsorController.loginToken);
          controller!.stopCamera();
          print(resp);
          if (resp[0]){
            Get.to(VoucherStatusPage(data:resp[1]));
          } else{
            Get.to(ScanVoucherPage());

          }

        }
      },
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      label: !isPressed
          ? Text(
              'Continue',
              style: TextStyle(color: Colors.white),
            )
          : Text(
              'Progress ...',
              style: TextStyle(color: Colors.white),
            ),
      icon:Icon(
        Icons.arrow_forward_ios_outlined,
        color: Colors.white,
      ),
      textColor: Colors.white,
      splashColor: Colors.red,
      color: !isPressed
          ?Colors.green:Colors.grey ,
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
  }

  @override
  void dispose() {
    print("Disposing first route");
    controller?.dispose();
    super.dispose();
  }

  Map<String, dynamic> jsondecoder(jsonString) {
    return jsonDecode(jsonString);
  }
}
