import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sponsor_app/controller/sponsor_controller.dart';
import 'package:sponsor_app/sizeconfig.dart';
import 'package:sponsor_app/views/base_page.dart';
import 'package:sponsor_app/utils/text_format.dart';
class HistoryVoucherEventList extends BasePage {
  HistoryVoucherEventList({Key? key}) : super(key: key);

  @override
  _HistoryVoucherEventListState createState() => _HistoryVoucherEventListState();
}

class _HistoryVoucherEventListState extends BasePageState<HistoryVoucherEventList> {
  SposnorController sposnorController = Get.find();
  @override
  Widget pageUI() {
    // TODO: implement pageUI
    return RefreshIndicator(
      onRefresh: () => sposnorController.getVoucherEvent(),
      child: SingleChildScrollView(
        physics: const AlwaysScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: Container(
          //color: Colors.black,
          height: MediaQuery.of(context).size.height,
          child: GetBuilder<SposnorController>(builder: (controller) {
            if (controller.voucherEvent.isEmpty)
              return Center(child: CircularProgressIndicator());

            return SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.horizontal,
              child: DataTable(
                  columns: [
                    DataColumn(label: TextWidget(text:"Event",context: context)),
                    DataColumn(label: TextWidget(text:"Voucher",context: context)),
                    DataColumn(label: TextWidget(text:"Participant #",context: context)),
                  ],
                  rows: controller.voucherEvent.map((element) {
                    return DataRow(cells: [
                      DataCell(Text(element.event.capitalizeFirstofEach)),
                      DataCell(Text(element.voucher.capitalizeFirstofEach)),
                      DataCell(Text('${element.participantNumber}')),
                    ]);
                  }).toList()),
            );
          }),
        ),
      ),
    );
  }
}

class TextWidget extends StatelessWidget {
  String text;
   TextWidget({
    Key? key,
    required this.context,
    required this.text,
  }) : super(key: key);

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Text(text.capitalizeFirstofEach, style: TextStyle(fontSize: getProportionateScreenHeight(24), fontWeight: FontWeight.bold, color: Theme.of(context).primaryColor),);
  }
}
