import 'package:get/get.dart';
import 'package:sponsor_app/models/city.dart';
import 'package:sponsor_app/models/event.dart';
import 'package:sponsor_app/models/voucher_event.dart';
import 'package:sponsor_app/models/voucher_model.dart';
import 'package:sponsor_app/services/api_service.dart';

class SposnorController extends GetxController {
  Map<int, int> pageNumber = {0: 1};
  Map<int, int> lastPage = {0: 0};
  Map<int, int> count = {0: 0};
  String loginToken = "";
  RxList<Voucher> vouchers = RxList<Voucher>();
  RxMap<int, List<Event>> mapEvents = RxMap<int, List<Event>>();
  var cityIsLoading = false;
  var voucherIsLoading = false.obs;
  RxList<City> cities = RxList<City>();
  String FCMToken="";
  List<VoucherEvent> voucherEvent = [];
  @override
  void init() {
    getCities();
    getVoucherEvent();
    registerDevice();
    
    
  }

Future getVoucherEvent() async {
    List data = await ApiService().getVoucherEvent(loginToken);
    if (data[0]) {
      voucherEvent = data[1];
    } else {
      voucherEvent = [];
    }

    update();
  }
  Future getCities() async {
    cityIsLoading = true;
    List data = await ApiService().getCities(loginToken);
    if (data[0]) {
      cities.value = data[1];
    } else {
      cities.value = [];
    }
   cityIsLoading=false;
    update();
  }

  initVars(city) {
    pageNumber[city] = 1;
    lastPage[city] = 0;
    count[city] = 0;
    mapEvents[city] = [];
  }
  Future<String> registerDevice() async {
    var message = await ApiService().registerDevice(FCMToken, loginToken);
    return message;
  }

  logOut() async {
    return await ApiService().logOut(loginToken);

}
grantVoucher(eventID, voucherList) async {
  return await ApiService().grantVoucher(eventID, voucherList, loginToken);

}
  getVouchers() async {
    voucherIsLoading.value = true;
    List result = await ApiService().getVouchers(loginToken);
    vouchers.value = result[0] ? result[1] : [];
    voucherIsLoading.value = false;
  }

  Future editVoucher(int id, int amount) async {
    return await ApiService().editVoucher(loginToken, id, amount);
  }

  Future addVoucher(String name, int quantity) async {
    return await ApiService().addVoucher(loginToken, name, quantity);
  }

  Future deleteVoucher(int id) async {
    return await ApiService().deleteVoucher(loginToken, id);
  }

  Future getAllEventswithPage(city) async {
    if (lastPage[city] != pageNumber[city]) {
      List data = await ApiService()
          .getAllEventswithPage(loginToken, pageNumber[city]!, city);
      if (pageNumber[city] == 1) {
        mapEvents[city] = data[1];
      } else {
        mapEvents[city]!.addAll(data[1]);
      }
      count[city] = data[0];
    }
    lastPage[city] = pageNumber[city]!;
    update();
  }
}
