import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class SizeConfig {  //Responsive class
  // Size calculation vars
  static late double screenWidth;
  static late double screenHeight;
  static late double _blockWidth = 0;
  static late double _blockHeight = 0;
  // Multipliers, will be used in the UI
  static late double textMultiplier;
  static late double imageSizeMultiplier;
  static late double heightMultiplier;
  static late double widthMultiplier;
  // Detects orientation
  static late bool isPortrait = true;
  static late bool isMobilePortrait = false;

  void init(BoxConstraints constraints, Orientation orientation) {
    
    if (orientation == Orientation.portrait) {  // If Portrait mode
      screenWidth = constraints.maxWidth;  // Device screen max avilable width
      screenHeight = constraints.maxHeight; // Device screen max avilable height
      isPortrait = true;
      if (screenWidth < 450) {  // Device is smartphone
        isMobilePortrait = true;
      }
    } else {                                    // Landscape mode
      screenWidth = constraints.maxHeight; // Actual height will be width in landscape
      screenHeight = constraints.maxWidth; // and vice versa
      isPortrait = false;
      isMobilePortrait = false;
    }

    // Calculate blocks
    _blockWidth = screenWidth / 100;
    _blockHeight = screenHeight / 100;

    // Asign values to multipliers
    textMultiplier = _blockHeight;
    imageSizeMultiplier = _blockWidth;
    heightMultiplier = _blockHeight;
    widthMultiplier = _blockWidth;


    // Print width and height for debugging and size calculation
    print(_blockWidth);
    print(_blockHeight);
  }
}

double getProportionateScreenHeight(double inputHeight) {
  double screenHeight = SizeConfig.screenHeight;
  // 812 is the layout height that designer use
  return (inputHeight / 812.0) * screenHeight;
}