import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:sponsor_app/models/city.dart';
import 'package:sponsor_app/models/event.dart';
import 'package:sponsor_app/models/voucher_event.dart';
import 'package:sponsor_app/models/voucher_model.dart';
import 'package:sponsor_app/views/login_view.dart';

//http://127.0.0.1:80/api/vouchers/

class ApiService {
  String baseUrl = ("http://192.168.1.103");

  final http.Client httpClient = http.Client();

  Future logOut(loginToken) async {
    var response = await httpClient.post(
      Uri.parse("$baseUrl/auth/logout/"),
      headers: {
        'Authorization': "Bearer $loginToken",
        "Content-Type": "application/json"
      },
    ).timeout(const Duration(seconds: 4));
    if (response.statusCode == 201) {
      Get.off(LoginView());
    }
  }

  Future<List> checkVoucher(id, token, loginToken) async {
    print('loginToken $loginToken');
    try {
      var response = await httpClient
          .post(Uri.parse("$baseUrl/api/check_voucher/"), headers: {
        'Authorization': "Bearer $loginToken",
      }, body: {
        'id': '$id',
        'token': token
      }).timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        Map<String, dynamic> data =
            json.decode(json.decode(utf8.decode(response.bodyBytes)));

        return [true, data];
      } else {
        var message = response.statusCode == 404
            ? "Sponsor unknown"
            : jsonDecode(response.body)['message'];

        Get.snackbar('Error ${response.statusCode}', message,
            duration: const Duration(seconds: 6),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING);
        return [false, null];
      }
      ;
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    }
  }
  Future<String> registerDevice(String token, String loginToken) async {
    try {
            var data = Map();
      data['registration_id'] = token;

    var response = await httpClient.post(Uri.parse(baseUrl + "/api/devices/",),
    body: jsonEncode(data),
     headers: {'Authorization' : "Bearer $loginToken", "Content-Type": "application/json"},).timeout(const Duration(seconds: 4));
    if (response.statusCode == 201) {
      return json.decode(response.body)["message"];
    } else {
      Get.snackbar('Error', 'Failed',
        duration: const Duration(seconds: 6),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
      return "Failed";
    }
  } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
        duration: const Duration(seconds: 6),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
        duration: const Duration(seconds: 6),
        snackPosition: SnackPosition.BOTTOM,
        snackStyle: SnackStyle.FLOATING);
        return "Failed";
    }
  }
  Future<bool> grantVoucher(eventID, voucherList, loginToken) async {
    try {
      var data = Map();
      data['event'] = eventID;
      data['voucher_list'] = voucherList;

      var response = await httpClient
          .post(Uri.parse("$baseUrl/api/grant/"),
              headers: {
                'Authorization': "Bearer $loginToken",
                "Content-Type": "application/json"
              },
              body: jsonEncode(data))
          .timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        var message = response.statusCode == 404
            ? "Sponsor unknown"
            : jsonDecode(response.body)['message'];

        Get.snackbar('Success', message,
            duration: const Duration(seconds: 6),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING);
        return true;
      } else {
        var message = response.statusCode == 404
            ? "Sponsor unknown"
            : jsonDecode(response.body)['message'];

        Get.snackbar('Error ${response.statusCode}', message,
            duration: const Duration(seconds: 6),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING);
        return false;
      }
      ;
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return false;
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return false;
    }
  }

  Future<List?> signIn(fbaseToken) async {
    try {
      final response = await httpClient.post(
        Uri.parse(baseUrl + '/api-firebase-auth/'),
        headers: {'Accept': 'application/json', 'JWT': fbaseToken},
      ).timeout(const Duration(seconds: 4));
      var data = json.decode(utf8.decode(response.bodyBytes));
      /* var data = json.decode(utf8.decode(response.bodyBytes)); */
      if (response.statusCode == 201) {
        //obtain refresh token
        var newResponse = await httpClient.post(
          Uri.parse(baseUrl + '/api/public_events/'),
          body: {"token": data["token"]},
          headers: {'Accept': 'application/json', 'Bearer': data["token"]},
        ).timeout(const Duration(seconds: 4));
        var newData = json.decode(utf8.decode(response.bodyBytes));

        return [true, newData["token"]];
      } else {
        return [false, null];
      }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
    }
  }

  Future<List> getVouchers(loginToken) async {
    try {
      var response = await httpClient.get(
        Uri.parse("$baseUrl/api/vouchers/"),
        headers: {'Authorization': "Bearer $loginToken"},
      ).timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        List data = json.decode(utf8.decode(response.bodyBytes));
        List<Voucher> vouchers = data.map((e) => Voucher.fromJson(e)).toList();
        print(vouchers);
        return [true, vouchers];
      } else {
        var message = response.statusCode == 404
            ? "Sponsor unknown"
            : jsonDecode(response.body)['message'];

        Get.snackbar('Error ${response.statusCode}', message,
            duration: const Duration(seconds: 6),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING);
        return [false, null];
      }
      ;
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    }
  }

  Future<List> addVoucher(loginToken, String name, int quantity) async {
    var data = Map();
    data['name'] = name;
    data['quantity'] = quantity;
    print(jsonEncode(data));
    try {
      var response = await httpClient.post(
        Uri.parse("$baseUrl/api/vouchers/"),
        body: jsonEncode(data),
        headers: {
          'Authorization': "Bearer $loginToken",
          "Content-Type": "application/json"
        },
      ).timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        Voucher voucher =
            Voucher.fromJson(json.decode(utf8.decode(response.bodyBytes)));
        return [true, voucher];
      } else {
        var message = response.statusCode == 404
            ? "Sponsor unknown"
            : jsonDecode(response.body)['message'];
        Get.snackbar('Error ${response.statusCode}', message,
            duration: const Duration(seconds: 6),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING);
        return [false, null];
      }
      ;
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    }
  }

  Future<List> editVoucher(loginToken, int id, int amount) async {
    var data = Map();
    data['amount'] = amount;
    print(jsonEncode(data));
    try {
      var response = await httpClient.post(
        Uri.parse("$baseUrl/api/vouchers/modify/$id"),
        body: jsonEncode(data),
        headers: {
          'Authorization': "Bearer $loginToken",
          "Content-Type": "application/json"
        },
      ).timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        Voucher voucher =
            Voucher.fromJson(json.decode(utf8.decode(response.bodyBytes)));
        return [true, voucher];
      } else {
        var message = response.statusCode == 404
            ? "Unknown"
            : jsonDecode(response.body)['message'];
        Get.snackbar('Error ${response.statusCode}', message,
            duration: const Duration(seconds: 6),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING);
        return [false, null];
      }
      ;
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    }
  }

  Future deleteVoucher(loginToken, int id) async {
    try {
      var response = await httpClient.delete(
        Uri.parse("$baseUrl/api/vouchers/$id"),
        headers: {'Authorization': "Bearer $loginToken"},
      ).timeout(const Duration(seconds: 4));
      if (response.statusCode == 204) {
        print(response.statusCode);
        if (response.body.isNotEmpty)
          Get.snackbar('Success', jsonDecode(response.body)['message'],
              duration: const Duration(seconds: 6),
              snackPosition: SnackPosition.BOTTOM,
              snackStyle: SnackStyle.FLOATING);
        return true;
      } else {
        var message = response.statusCode == 404
            ? "Sponsor unknown"
            : jsonDecode(response.body)['message'];

        Get.snackbar('Error ${response.statusCode}', message,
            duration: const Duration(seconds: 6),
            snackPosition: SnackPosition.BOTTOM,
            snackStyle: SnackStyle.FLOATING);
        return false;
      }
      ;
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return false;
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return false;
    }
  }

  getAllEventswithPage(String loginToken, int pageNumber, int city) async {
    try {
      String url = city == 0
          ? "/api/public_events/?page_number=$pageNumber"
          : "/api/public_events/?page_number=$pageNumber&city=$city";

      var response = await httpClient.get(
        Uri.parse(baseUrl + url),
        headers: {'Authorization': "Bearer $loginToken"},
      ).timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        List jsonResponse = json.decode(response.body)["results"];
        int count = json.decode(response.body)["count"];
        List<Event> listEvent =
            jsonResponse.map((item) => Event.fromJson(item)).toList();
        print(listEvent);
        return [count, listEvent];
      } else {
        print("response.statusCode ${response.statusCode}");
      }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    }
  }

  getCities(String loginToken) async {
    try {
      var response = await httpClient.get(
        Uri.parse(baseUrl + "/api/cities/"),
        headers: {'Authorization': "Bearer $loginToken"},
      ).timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        List jsonResponse = json.decode(response.body);
        List<City> listCity =
            jsonResponse.map((item) => City.fromJson(item)).toList();
        print(listCity);
        return [true, listCity];
      } else {
        print("response.statusCode ${response.statusCode}");
      }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    }
  }

    getVoucherEvent(String loginToken) async {
    try {
      var response = await httpClient.get(
        Uri.parse(baseUrl + "/api/voucher_event/"),
        headers: {'Authorization': "Bearer $loginToken"},
      ).timeout(const Duration(seconds: 4));
      if (response.statusCode == 200) {
        List jsonResponse = json.decode(utf8.decode(response.bodyBytes));
        List<VoucherEvent> listVoucherEvent =
            jsonResponse.map((item) => VoucherEvent.fromJson(item)).toList();
        print(listVoucherEvent);
        return [true, listVoucherEvent];
      } else {
        print("response.statusCode ${response.statusCode}");
      }
    } on TimeoutException catch (_) {
      print("TimeOut Error");
      Get.snackbar('Error', 'TimeOut Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    } on SocketException catch (_) {
      print("Connection Error");
      Get.snackbar('Error', 'Connection Error',
          duration: const Duration(seconds: 6),
          snackPosition: SnackPosition.BOTTOM,
          snackStyle: SnackStyle.FLOATING);
      return [false, null];
    }
  }
}
