class Voucher {
  Voucher({
    required this.name,
    required this.id,
    required this.quantity,
    required this.available,
    required this.sponsor,
    required this.shortcut,
  });
  late final String name;
  late final int id;
  late final int quantity;
  late final int available;
  late final String sponsor;
  late final String shortcut;

  Voucher.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    id = json['id'];
    quantity = json['quantity'];
    available = json['available'];
    sponsor = json['sponsor'];
    shortcut = json['shortcut'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['id'] = id;
    _data['quantity'] = quantity;
    _data['available'] = available;
    _data['sponsor'] = sponsor;
    _data['shortcut'] = shortcut;
    return _data;
  }
}
