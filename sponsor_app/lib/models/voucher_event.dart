class VoucherEvent {
  VoucherEvent({
    required this.id,
    required this.participantNumber,
    required this.voucher,
    required this.event,
  });
  late final int id;
  late final int participantNumber;
  late final String voucher;
  late final String event;
  
  VoucherEvent.fromJson(Map<String, dynamic> json){
    id = json['id'];
    participantNumber = json['participantNumber'];
    voucher = json['voucher'];
    event = json['event'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['participantNumber'] = participantNumber;
    _data['voucher'] = voucher;
    _data['event'] = event;
    return _data;
  }
}