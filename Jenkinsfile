pipeline {

  environment {
    PROJECT = "arcane-transit-306018"
    APP_NAME = "gads-community"
    FE_SVC_NAME = "${APP_NAME}-frontend"
    BE_SVC_NAME = "${APP_NAME}-backend"
    CLUSTER = "jenkins-cd"
    CLUSTER_ZONE = "us-central1-a"
    BACKEND_IMAGE_TAG = "gcr.io/${PROJECT}/${BE_SVC_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
    FRONTEND_IMAGE_TAG = "gcr.io/${PROJECT}/${FE_SVC_NAME}:${env.BRANCH_NAME}.${env.BUILD_NUMBER}"
    JENKINS_CRED = "${PROJECT}"
    NAMESPACE = "production"
    SQLINSTANCE = "postgresql-db3"

  }

  agent {
    kubernetes {
      label 'GADS-app'
      defaultContainer 'jnlp'
      yaml """
apiVersion: v1
kind: Pod
metadata:
labels:
  component: ci
spec:
  # Use service account that can deploy to all namespaces
  serviceAccountName: cd-jenkins
  containers:
  - name: gcloud
    image: gcr.io/cloud-builders/gcloud
    command:
    - cat
    tty: true
  - name: kubectl
    image: gcr.io/cloud-builders/kubectl
    command:
    - cat
    tty: true
"""
}
  }
  stages {
    stage('Build and push the Backend image with Container Builder') {
      steps {
        container('gcloud') {
          sh("PYTHONUNBUFFERED=1 gcloud builds submit -t ${BACKEND_IMAGE_TAG} ./backend/")
        }
      }
    }
    stage('Check sercrets') {

      steps{
          script{
            if (env.BRANCH_NAME == 'master'){
                NAMESPACE = 'production'
            } else {
                NAMESPACE = env.BRANCH_NAME
            }
          }
        container('kubectl') {
          sh("gsutil cp gs://secrets-gads-community-project/secrets/* .")
          sh("kubectl get ns ${NAMESPACE} || kubectl create ns ${NAMESPACE}")
          sh("kubectl get configmap app-config -n ${NAMESPACE} || kubectl apply -f ./app_config_configmap.yaml -n ${NAMESPACE} ")
          sh("kubectl get secret app-secret -n ${NAMESPACE} || kubectl create secret generic app-secret --from-env-file=./django_secrets -n ${NAMESPACE}")
          sh("kubectl get secret tls-certificate -n ${NAMESPACE} || kubectl create secret tls tls-certificate --key ./nginx-selfsigned.key --cert ./nginx-selfsigned.crt -n ${NAMESPACE}")
          sh("kubectl get configmap connectionname -n ${NAMESPACE} || kubectl --namespace production create configmap connectionname --from-literal=connectionname=`gcloud sql instances describe '${SQLINSTANCE}' --format='value(connectionName)'` -n ${NAMESPACE}")
          sh("kubectl get secret cloudsql-sa-creds -n ${NAMESPACE} || kubectl create secret generic cloudsql-sa-creds --from-file=credentials.json=./credentials.json -n ${NAMESPACE} ")
        }
      }
    
    }
    stage('Deploy Backend in Production') {
      // Production branch
      when { branch 'master' }
      steps{
        container('kubectl') {
        // Change deployed image in canary to the one we just built
          sh("sed -i.bak 's#gcr.io/arcane-transit-306018/django#${BACKEND_IMAGE_TAG}#' ./k8s/backend/production/backend_deplyment.yaml")
          step([$class: 'KubernetesEngineBuilder', namespace:'production', projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/backend/services', credentialsId: env.JENKINS_CRED, verifyDeployments: false])
          step([$class: 'KubernetesEngineBuilder', namespace:'production', projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/backend/production', credentialsId: env.JENKINS_CRED, verifyDeployments: true])
        }
      }
    }
    stage('Build and push the Frontend image with Container Builder') {
      steps {
        container('gcloud') {
          sh("PYTHONUNBUFFERED=1 gcloud builds submit -t ${FRONTEND_IMAGE_TAG} ./frontend/")
        }
      }
    }
    stage('Deploy Frontend in Production') {
      // Production branch
      when { branch 'master' }
      steps{
        container('kubectl') {
        // Change deployed image in canary to the one we just built
          sh("sed -i.bak 's#gcr.io/frontend#${FRONTEND_IMAGE_TAG}#' ./k8s/frontend/production/frontend_deployment.yaml")
          step([$class: 'KubernetesEngineBuilder', namespace:'production', projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/frontend/services', credentialsId: env.JENKINS_CRED, verifyDeployments: false])
          step([$class: 'KubernetesEngineBuilder', namespace:'production', projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/frontend/production', credentialsId: env.JENKINS_CRED, verifyDeployments: true])
          sh("echo http://`kubectl --namespace=production get service/${FE_SVC_NAME} -o jsonpath='{.status.loadBalancer.ingress[0].ip}'` > ${FE_SVC_NAME}")
        }
      }
    }


    stage('Deploy Backend in new feature development namespace') {
      // Developer Branches
      when {
        not { branch 'master' }
        not { branch 'canary' }
      }
      steps {
        container('kubectl') {
          // Create namespace if it doesn't exist
          sh("kubectl get ns ${env.BRANCH_NAME} || kubectl create ns ${env.BRANCH_NAME}")
          sh("sed -i.bak 's#gcr.io/arcane-transit-306018/django#${BACKEND_IMAGE_TAG}#' ./k8s/backend/production/backend_deplyment.yaml")
          step([$class: 'KubernetesEngineBuilder', namespace:"${env.BRANCH_NAME}", projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/backend/services', credentialsId: env.JENKINS_CRED, verifyDeployments: false])
          step([$class: 'KubernetesEngineBuilder', namespace:"${env.BRANCH_NAME}", projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/backend/production', credentialsId: env.JENKINS_CRED, verifyDeployments: true])
        }
      }
    }
  
    stage('Deploy Frontend in new feature development namespace') {
      // Developer Branches
      when {
        not { branch 'master' }
        not { branch 'canary' }
      }
      steps {
        container('kubectl') {
          // Don't use public load balancing for development branches
          sh("sed -i.bak 's#LoadBalancer#ClusterIP#' ./k8s/frontend/production/frontend_deployment.yaml")
          sh("sed -i.bak 's#gcr.io/frontend#${FRONTEND_IMAGE_TAG}#' ./k8s/frontend/production/frontend_deployment.yaml")
          step([$class: 'KubernetesEngineBuilder', namespace:"${env.BRANCH_NAME}", projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/frontend/services', credentialsId: env.JENKINS_CRED, verifyDeployments: false])
          step([$class: 'KubernetesEngineBuilder', namespace:"${env.BRANCH_NAME}", projectId: env.PROJECT, clusterName: env.CLUSTER, zone: env.CLUSTER_ZONE, manifestPattern: 'k8s/frontend/production', credentialsId: env.JENKINS_CRED, verifyDeployments: true])
        }
      }
  }
}
}